<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\RegisterController@login');
Route::post('edit', 'Api\RegisterController@edit');
Route::post('enviar', 'Api\RegisterController@enviar');
Route::post('actualizar', 'Api\RegisterController@actualizar');
Route::get('logout', 'Api\RegisterController@logout');
Route::post('contactanos', 'Api\RegisterController@contactanos');

Route::get('eventos', 'Api\EventController@index');
Route::get('futuros/{id}', 'Api\EventController@futuros');
Route::get('pasados/{id}', 'Api\EventController@pasados');
Route::get('modificar/{friends}/{venta}', 'Api\EventController@friends');
Route::get('venta/{id}', 'Api\EventController@venta');
Route::get('eventos/{id}', 'Api\EventController@show');
Route::get('entradas', 'Api\EntradaController@index');
Route::get('entradas/{id}', 'Api\EntradaController@show');
Route::get('obtener/{preventa}', 'Api\VentaController@obtener');
Route::get('user/{id}', 'Api\VentaController@user');
Route::get('porcentaje/{puntos}', 'Api\VentaController@porcentaje');
Route::get('puntos', 'Api\VentaController@puntos');
Route::get('scanner/{code}', 'Api\VentaController@scanner');
Route::get('pagar/{code}', 'Api\VentaController@pagar');

Route::post('venta', 'Api\VentaController@saveVenta');
