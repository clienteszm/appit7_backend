<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('bienvenida', function () {
    return view('emails.prueba');
});

Route::get('politica-de-privacidad', function () {
    return view('politica');
});

Route::get('politica-de-cookies', function () {
    return view('cookies');
});

Route::get('ap/{id}', 'Api\RegisterController@aprobado');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
