<link rel="stylesheet" href="{{ url('css/app.css') }}">
<title>It7 Lounge Club</title>
<link rel="icon" href="{{ asset('images/favicon.ico') }}"  type="image/x-icon"/>
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        padding: 0px 5vh 0px 5vh;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('images/logo.png') }}" alt="" style="text-align:center; width: 5%;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff; font-size:35px;">Política de Cookies</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 20px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
              </p>
              </div>
              <div class="body">
                <div class="" style="padding-right: 25px; display: inline-table;">
                  <div class="detail__items__contenido" >
                    <p style="text-align:justify;">
                      <strong>
                        Al visitar nuestro app, crear una cuenta o comprar entradas,
                        usted acepta que tanto nosotros como diversas organizaciones de terceros utilicemos
                        cookies de conformidad con su configuración de cookies. Si desea información sobre cómo se utiliza su
                        información personal, por favor, vea nuestra <a href="{{ url('politica-de-privacidad') }}">Política de privacidad</a>.
                      </strong>
                    </p>
                    <p style="text-align:justify;">
                      <strong>¿Que son las cookies?</strong>
                      <br>
                      Una cookie es un pequeño archivo de texto— en el que se contiene una reducida cantidad de información— que se instala en su ordenador a través de su navegador, de forma que el sitio web pueda recordar quién es usted.
                      <br>
                      <br>
                      El periodo de tiempo durante el que una cookie permanecerá en su ordenador dependerá de si se trata de una cookie permanente o de sesión. Las cookies de sesión son cookies temporales que permanecen en su ordenador hasta que usted abandone el sitio web. Las cookies permanentes continúan instaladas en su ordenador después de que usted haya terminado de navegar, hasta que las mismas expiren o sean eliminadas.
                      <br>
                      <br>
                      Un “pixel tag”, también denominado “web beacon” está formado por una imagen invisible con una línea de código que es incluida en un mensaje de correo electrónico o en una página web. Las cookies pueden categorizarse como cookies Propias o de Terceros. Las “Cookies Propias” son cookies que IT7 Lounge Club instala en su dispositivo, mientras que “Cookies de Terceros” son cookies controladas por un tercero pero instaladas en su ordenador cuando visita nuestro sitio. Por ejemplo, utilizamos cookies de Google Analytics para recabar información sobre la experiencia de los usuarios en nuestro sitio web.
                    </p>

                    <p style="text-align:justify;">
                      <strong>
                        ¿Para qué se utilizan las cookies?
                      </strong>
                      <br>
                      Las cookies pueden incluirse en una de las siguientes categorías: cookies estrictamente necesarias; analíticas, funcionales y publicitarias. El siguiente cuadro le informa con mayor detalle sobre cada una de las categorías.
                      <br>
                      <br>
                      <strong>- Cookies Estrictamente Necesarias</strong>
                      <br>
                      Estas cookies son necesarias para que nuestro sitio web funcione. Permiten que usted pueda navegar por el sitio y utilizar sus funcionalidades. Sin estas cookies no podrían prestarse los servicios necesarios, tales como la capacidad de acceder a áreas seguras.
                      Por ejemplo, mantenerle conectado durante su visita. Sin estas cookies el sitio no le recordaría, por lo que tendría que iniciar sesión continuamente. Cuando compre entradas, estas cookies garantizan que las mismas permanezcan en su cesta de la compra cuando acceda a la página de pago.
                      <br>
                      <br>
                      <strong>- Cookies Funcionales</strong>
                      <br>
                      Estas cookies recogen información sobre el tipo de uso que las personas realizan de nuestro sitio como, por ejemplo, qué páginas son las más frecuentemente visitadas, y cómo se desplazan de un vínculo a otro. La información recogida en nuestro sitio abierto es agrupada de forma conjunta con información sobre el uso de otras personas de nuestro sitio. Cuando usted acceda al portal del cliente, podremos asociar información de las cookies con su cuenta. En líneas generales, estas cookies nos proporcionan información analítica sobre cómo funciona nuestro sitio web y cómo podemos mejorarlo.
                      <br>
                      <br>
                      <strong>- Cookies Analíticas</strong>
                      <br>
                      Estas cookies nos permiten recordar las opciones que usted haya elegido y personalizar nuestro sitio web para proporcionarle funciones y contenidos optimizados. Por ejemplo, estas cookies pueden utilizarse para recordar su nombre de usuario (pero no su contraseña); también pueden usarse para recordar los cambios que usted haya realizado al tamaño o a la fuente del texto, y a otras partes de las páginas que usted pueda personalizar.
                      <br>
                      <br>
                      <strong>- Cookies Publicitarias</strong>
                      <br>
                      Estas cookies se utilizan para remitir mensajes de marketing y publicidad, personalizados para usted en función de las páginas que haya visitado, de sus compras o de aquello sobre lo que haya mostrado interés. Usamos estas cookies de forma conjunta con las organizaciones de terceros que proporcionan herramientas que nos permiten agruparle con otros usuarios para dirigir estos mensajes y esta publicidad. Por ejemplo, cuando navegue por nuestros sitios web o apps, añada entradas a su cesta o realice una compra, podrá ver posteriormente anuncios o mensajes de estos eventos o de eventos similares. Lo anterior podrá incluir un anuncio en Facebook, resultados de búsquedas en Google o un correo electrónico remitido por nosotros recordándole que sigue teniendo entradas en su cesta. Todos los terceros con los que trabajamos cumplen con los principios del sector sobre publicidad online basada en el comportamiento (online behavioural advertising –OBA-) para garantizar que usted no sea identificado personalmente y que tenga la opción de causar baja voluntaria en el futuro.
                      <br>
                      <br>
                    </p>

                    <p style="text-align:justify;">
                      <strong>¿Cómo puede gestionar las cookies de este sitio?</strong>
                      <br>
                      La mayoría de los buscadores le permitirán gestionar sus preferencias de cookies; p. ej., que el navegador le notifique cuando reciba una nueva cookie o utilizarlo para desactivar completamente las cookies. Si usted decide desactivarlas o eliminarIas totalmente, algunos sitios web no funcionarán, pues para prestarle el servicio que usted haya solicitado se basan en cookies (véase el apartado anterior sobre Cookies Estrictamente Necesarias).
                      <br>
                      <br>
                      Si no desea permitir que nosotros y organizaciones de terceros utilicemos cookies en nuestros correos electrónicos, tales como pixel tags, la mejor forma para hacerlo es no permitir imágenes cuando vea nuestros correos electrónicos. Dicho de otra forma, solamente el texto sin formato del correo electrónico. Algunos buscadores y clientes de correos electrónicos cuentan con configuraciones o extensiones para desactivar / bloquear estas cookies como, por ejemplo, Gmail.
                    </p>

                    <p style="text-align:justify;">
                      <strong>Actualizaciones a esta política</strong>
                      <br>
                      En cualquier momento podremos actualizar esta política; por favor, visítela periódicamente para comprobar si se ha producido alguna modificación.
                      <br>
                      <br>
                      Última actualización de la política [07/2020]
                    </p>
                  </div>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
