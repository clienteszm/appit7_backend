<link rel="stylesheet" href="{{ url('css/app.css') }}">
<title>It7 Lounge Club</title>
<link rel="icon" href="{{ asset('images/favicon.ico') }}"  type="image/x-icon"/>
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        padding: 0px 5vh 0px 5vh;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('images/logo.png') }}" alt="" style="text-align:center; width: 5%;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff; font-size:35px;">Política de Privacidad</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 20px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
              </p>
              </div>
              <div class="body">
                <div class="" style="padding-right: 25px; display: inline-table;">
                  <div class="detail__items__contenido" >
                      <p style="text-align: justify">
                        <strong>Nuestro Compromiso Frente A Ti</strong>
                        <br>
                        Tu eres el núcleo de todo lo que hacemos. Nuestro objetivo es mantener tu confianza a través de una respetuosa gestión de tu información personal y otorgándote a ti el control de la misma.
                        <br>
                        <br>
                        Es importante que sepas qué información personal IT7 Lounge Club recoge sobre ti, y cómo la utilizamos.
                        <br>
                        <br>
                        Hemos hecho todo lo posible para que nuestras explicaciones sean breves y de fácil comprensión. No obstante, si quisieras información adicional o tuvieras cualquier pregunta o duda, por favor, contacta con nuestro Delegado de Protección de Datos conforme a los datos incluidos en la sección ‘Contacto’ más adelante.
                        <br>
                        <br>
                        Te informaremos adecuadamente si en cualquier momento realizamos algún cambio sustancial en nuestras prácticas de privacidad. Si fuera necesario también te pediremos tu permiso.
                      </p>

                      <p style="text-align: justify">
                        <strong>Notificación De Privacidad</strong>
                        <br>
                        Nuestra Notificación de Privacidad ha sido diseñada pensando en ti. La forma en la que se te vaya a aplicar la notificación dependerá de cómo interactúes con nosotros. Por ejemplo, si tu:
                        <br>
                        <br>
                        1. Nos compras una entrada, utilizaremos la información que nos suministres para cumplir tanto con nuestras obligaciones como con las de los Socios de Evento frente a ti en la prestación de dicho servicio, y, cuando se encuentre permitido, para mantenerte informado sobre otros eventos que puedan ser de tu interés; y
                        <br>
                        <br>
                        2. Cuando navegues por nuestros sitios usaremos cookies para personalizar tu experiencia y para, esperamos, proporcionarte una experiencia perfecta.
                        <br>
                        <br>
                        Tus opciones y derechos al amparo de cada escenario se explican más detalladamente a continuación.
                      </p>

                      <p style="text-align: justify">
                        <strong>Qué Información Tenemos y Dónde la Obtenemos</strong>
                      </p>

                      <ul style="text-align: justify">
                        <li>
                          Cuando creas una cuenta, compras una entrada o tengas una entrada que te haya transmitido un amigo, recogeremos tu información que, en función del servicio que estemos prestando, podrá incluir tu información de contacto y de facturación.
                        </li>
                        <li>
                          Cuando utilices nuestros sitios web o app, recogeremos información como, por ejemplo, el navegador y dispositivo que estés utilizando, tu dirección IP, tu localización, el sitio desde el que has accedido, para qué has utilizado y para qué no has utilizado nuestro sitio/app, o el sitio al que accedas tras visitar el nuestro. Para información adicional sobre cómo recogemos esta información, consulta nuestra <a href="{{ url('politica-de-cookies') }}">Política de Cookies</a>.
                        </li>
                        <li>
                          Cuando utilices alguna función de redes sociales dentro de nuestro sitio web o app y realices alguna publicación en redes sociales, el sitio de la red social nos suministrará algo de información sobre ti.
                        </li>
                        <li>
                          Si tienes algún requisito de accesibilidad, queremos asegurarnos de que disfrutas de la mejor experiencia cuando asistas a eventos. Para ello, necesitaremos recoger datos de tus requisitos (que pueden implicar que nos suministres información sobre tu salud mental o física).
                        </li>
                        <li>
                          En las contadas ocasiones en las que recogemos información personal de niños, siempre solicitamos el consentimiento paterno y, en todo caso, recogeremos esa información exclusivamente a los efectos especificados en el momento de su recogida.
                        </li>

                      </ul>

                      <p style="text-align: justify">
                        <strong>
                          Cómo Utilizamos Tu Información y Por Qué
                        </strong>
                        <br>
                        Las cookies pueden incluirse en una de las siguientes categorías: cookies estrictamente necesarias; analíticas, funcionales y publicitarias. El siguiente cuadro le informa con mayor detalle sobre cada una de las categorías.
                        <br>
                        <br>
                        <strong>1. Para el cumplimiento de nuestro contrato contigo</strong>
                        <br>
                        Utilizamos tu información cuando suscribas un contrato con nosotros (por ejemplo cuando compres una entrada) de forma que podamos:
                      </p>
                      <ul style="text-align: justify">
                        <li>
                          Procesar tu pedido
                        </li>
                        <li>
                          Recibir el pago, y
                        </li>
                        <li>
                          Proporcionarte atención al cliente.
                        </li>
                      </ul>
                      <p style="text-align: justify">
                        <strong>2. Para nuestros legítimos intereses comerciales</strong>
                      </p>
                      <ul style="text-align: justify">
                        <li>
                        Para realizar investigaciones y análisis de mercado que nos ayuden a mejorar y personalizar nuestros productos y servicios.
                        </li>
                        <li>
                        A efectos de nuestras acciones de marketing, a menos que dicho marketing requiera tu consentimiento (consulta la sección 3 siguiente).
                        </li>
                        <li>
                        Para remitirte correos electrónicos de atención al cliente, incluidas confirmaciones de reservas y recordatorios de eventos.
                        </li>
                        <li>
                        Para prevenir o detectar comportamientos ilícitos, para proteger o hacer cumplir nuestros legítimos derechos o conforme lo permita de otra forma la ley. Por ejemplo, para asegurarnos de que las entradas son recibidas por auténticos fans. Por lo tanto, podremos usar tu información para impedir la reventa de entradas, un uso indebido de nuestra propiedad intelectual e industrial (p. ej., nuestras marcas o las de nuestros Socios de Evento), fraudes, u otros delitos.
                        </li>
                        <li>
                        Para garantizar la seguridad de nuestras operaciones y de las operaciones de nuestros Socios de Evento.
                        </li>
                        <li>
                          Para crear un perfil sobre ti que nos ayude a personalizarte nuestros servicios, y poder participar de descuentos especiales en la compra de tus entradas.

                        </li>
                      </ul>
                      <p style="text-align: justify">
                        <strong>En relación con tus ajustes de personalización</strong>
                        <br>
                        Si te conocemos mejor, podremos darte más de lo que desees. Para asegurarnos de que nuestros mensajes y sitio web son relevantes para ti, creamos un perfil de usuario con la información que conozcamos sobre ti y sobre cómo utilizas nuestros servicios.
                        <br>
                        <br>
                        <!-- <strong>Cómo desactivar la personalización</strong>
                        <br>
                        Simplemente desactiva “Permitir personalización” en los ajustes de tu cuenta. Pararemos cualquier personalización y solamente utilizaremos tus datos para servicios esenciales, tales como detección de fraude o de actividades de reventa – y para cualquier preventa de Fan Verificado. Si no tienes una cuenta podrás contactar con nosotros para la desactivación.
                        <br>
                        <br> -->
                        <!-- <strong>¿Qué ocurre si desactivas la personalización?</strong>
                        <br>
                        Ya no utilizaremos tus datos para determinar qué podría interesarte. Esto significa que no recibirás ninguna recomendación personal y que cualquier boletín informativo al que te hayas suscrito será genérico (aunque si hubieras solicitado recibir alertas sobre artistas o recintos concretos, seguirás recibiendo dicha información).
                        <br>
                        <br>
                        <strong>¿Es lo mismo desactivar la personalización que desactivar las cookies?</strong>
                        <br>
                         No, son independientes. Para controlar tus ajustes de cookies podrás utilizar nuestra herramienta de aceptación de cookies. Consulta nuestra <a (click)="presentCookies()">Política de Cookies </a> para información adicional.
                         <br>
                         <br> -->
                        <strong>3. Cuando nos hayas dado tu consentimiento</strong>
                        <br>
                      </p>
                      <ul style="text-align: justify">
                        <li>
                          Para ponernos en contacto contigo con información u ofertas sobre nuestros próximos eventos, productos o servicios – estas comunicaciones se podrán realizar a través de correo electrónico, notificaciones push y web, mediante SMS, o redes sociales. Podrás cambiar tus preferencias de marketing en cualquier momento; para ello, consulta la sección “Tus opciones y derechos” más adelante.
                        </li>
                        <!-- <li>
                          Para prestarte servicios basados en la localización – como, por ejemplo, a través de nuestras app de festival, que te permiten ver dónde te encuentras en el mapa, de forma que podamos usar tu localización para remitirte notificaciones push sobre lo que ocurre a tu alrededor.
                        </li> -->
                        <li>
                          Para remitir comunicaciones de publicidad y marketing personalizadas en nuestros sitios web y app (véase nuestra <a (click)="presentCookies()">Política de Cookies</a> para información adicional)
                        </li>
                        <li>
                          Para tratar los datos sobre tu salud con el fin de cumplir con tus requisitos de accesibilidad, cuando sea específicamente solicitado y se otorgue un consentimiento expreso.
                        </li>
                      </ul>

                      <p style="text-align: justify">
                        <strong>Con Quién Compartimos Tus Datos y Por Qué</strong>
                      </p>
                      <ul  style="text-align: justify">
                        <li>
                          Con nuestros terceros proveedores de servicios (en ocasiones conocidos como encargados del tratamiento de datos), tales como los proveedores de informática en la nube que proporcionan la infraestructura de TI sobre la que se establecen nuestros productos y sistemas.
                        </li>
                        <li>
                          Podremos compartir tu información con nuestros Socios de Eventos, de forma que puedan organizar el evento y por otros motivos descritos en sus políticas de privacidad. Siempre citaremos a los Socios de Eventos cuando compres una entrada y se te dará la opción de suscribirte para recibir comunicaciones de marketing de los mismos.
                        </li>
                        <li>
                          Si compras o vendes entradas a través de nuestra plataforma de reventa, podremos revelar tu información al comprador o vendedor (según corresponda) a efectos de cumplimiento del pedido – es decir, para que tu obtengas la entrada que hayas comprado o cobres la entrada que hayas vendido.
                        </li>
                        <li>
                          Con terceros que suministren los productos y servicios comprados por ti (p. ej., seguros de entradas o mercadería) de forma que puedan procesar y cumplir con tus pedidos.
                        </li>
                        <li>
                          Con agencias de la Administración u otros órganos autorizados, cuando se encuentre legalmente permitido o requerido.
                        </li>
                        <li>
                          Con cualquier sucesor de todo o de parte de nuestro negocio.
                        </li>
                      </ul>

                      <p style="text-align: justify">
                        <strong>Tus Opciones y Derechos</strong>
                        <br>
                        <strong>Tus opciones</strong>
                        <br>
                        Cuando nos hayas dado tu consentimiento podrás retirarlo conforme a lo siguiente.
                      </p>

                      <ul style="text-align: justify">
                        <li>
                          Para dejar de recibir nuestras comunicaciones de marketing podrás cambiar tus preferencias en tu cuenta, seguir las instrucciones de cancelación de suscripción en cualquiera de los correos electrónicos que le enviemos, o ponerte en contacto con nosotros y lo haremos por ti.
                        </li>
                        <li>
                          Para optar por causar baja en el uso de cookies y de herramientas de seguimiento, por favor, consulta nuestra <a (click)="presentCookies() ">Política de Cookies</a>.
                        </li>
                        <!-- <li>
                          Para optar por causar baja en el seguimiento de localización y en las notificaciones push, podrás cambiar los ajustes en tu dispositivo o mantener desactivada tu localización. Para dejar de recibir notificaciones push de red tendrás que utilizar los ajustes de tu navegador.
                        </li> -->
                        <li>
                          Para oponerte a la personalización, podrás cambiar tus preferencias en tu cuenta. Si esta opción no está disponible podrás contactar con nosotros y lo haremos por ti
                        </li>
                      </ul>

                      <p style="text-align: justify">
                        <strong>Tus derechos</strong>
                        <br>
                        También cuentas con derechos sobre cómo se utiliza tu información personal, incluidos los siguientes:
                      </p>
                      <ul  style="text-align: justify">
                        <li>
                         Derecho a oponerse a nuestro tratamiento de tus datos.
                        </li>
                        <li>
                         Derecho a solicitar que tu información sea eliminada o su uso futuro restringido.
                        </li>
                        <li>
                         Derecho a solicitar una copia de la información que tengamos sobre ti.
                        </li>
                        <li>
                         Derecho a corregir, modificar o actualizar la información que nos hayas suministrado (cuando tengas una cuenta con nosotros también podrás hacerlo accediendo a la misma y actualizando tu información).
                        </li>
                        <li>
                         Derecho a impugnar cualquier decisión automática que adoptemos sobre ti. Una decisión automática es una decisión adoptada sin ninguna intervención humana que tiene consecuencias jurídicas (p. ej., verificación de crédito). Normalmente no adoptamos decisiones automáticas, pero cuando lo hagamos dejaremos claro que se está adoptando ese tipo de decisión.
                        </li>
                      </ul>
                      <p  style="text-align: justify">
                         Para ejercer cualquiera de los derechos anteriores, por favor contacta con nosotros.

                         <!-- Ten en cuenta, por favor, que si bien valoramos cuidadosamente cada solicitud que recibimos, tus derechos podrán diferir en función de su lugar de residencia y no siempre vendremos obligados a su cumplimiento. En tal caso, te explicaremos por qué. Ten en cuenta que en ciertos casos las entradas para asistir a determinados eventos son vendidas a través de promotores que colaboran con IT7 Lounge Club, en cuyo caso la compra de entradas se realizará a través de la página web del promotor, ajena a IT7 Lounge Club. Por ello, te informamos que en esos casos nuestra web puede incluir, mostrar o desplegar enlaces a las páginas web de los promotores. Dichas páginas web operan independientemente de nosotros. Estas páginas tienen sus propias políticas de privacidad, por lo que te recomendamos encarecidamente que las leas cuando las visites. En la medida en que cualquier página web con enlace que visites no sea de nuestra propiedad ni esté bajo nuestro control, no somos responsables del contenido de tales sitios web, su uso o sus prácticas de privacidad. -->
                      </p>

                      <p style="text-align: justify">
                        <strong>Contacta Con Nosotros</strong>
                        <br>
                        Si tuvieras cualquier duda o cuestión sobre lo anterior o sobre nuestro enfoque a la privacidad, nuestra Oficina de Privacidad, incluido nuestro Delegado de Protección de Datos, estaremos encantados de ayudarte: <a href="mailto:privacy@it7loungeclub.com">privacy@it7loungeclub.com</a>
                        <br>
                        <br>
                        También puedes acudir a Agencia de Protección de Datos, si bien te animamos a que primero nos permitas ayudarte.
                        <br>
                        <br>
                      </p>

                    </div>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
