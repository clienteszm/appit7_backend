<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        border: 1px solid #EBF0F0;
                                                        box-sizing: border-box;
                                                        padding: 30px 30px 30px 30px;
                                                        box-shadow: 0px 4px 27px rgba(1, 15, 38, 0.04);
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('images/logo.png') }}" alt="" style="text-align:center; width: 8%;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff"></span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 18px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
                Código de validación de cuenta
              </p>
              </div>
              <div class="body">
                <div class="" style="   height: 28vh;
                                        align-items: center;
                                        display: grid;
                                        align-content: center;">
                  <p style="font-size:18px; font-weight: bold;">Ingresa el siguiente código para continuar con la recuperación de tu cuenta:</p>
                   <strong style="font-size:16px; text-align: center; color: #e73569;">{{ $data->codigo }}</strong>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh!important;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;!important" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
