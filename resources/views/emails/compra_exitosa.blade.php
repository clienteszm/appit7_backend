<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('images/logo.png') }}" alt="" style="text-align:center; width: 12%;">
                  </span>
                </p>
                <h3 style="color:#fff; font-size: 15px;">Hola <span style="text-transform: capitalize; color:#ffff">{{ $data['nombre'] }}</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 18px;
                                        margin-top: -18px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
                ¡Felicidades! tu compra ha sido exitosa.
              </p>
              </div>
              <div class="body">
                <div style="padding-right: 25px;
                            display: inline-table;">
                  <p style="font-size:20px; font-weight: bold;">Detalles de tu entrada</p>
                  <ul style="list-style: none;
                      text-align: justify;">
                    <li style="color:#000 !important;">
                     <strong> Evento</strong>
                     <br>
                     <p>{{ $data['evento'] }}</p>
                    </li>
                    <li style="color:#000 !important;">
                     <strong> Fecha</strong>
                     <br>
                     <p>{{ $data['fecha'] }}</p>
                    </li>
                    <li style="color:#000 !important;">
                     <strong> Hora</strong>
                     <br>
                     <p>{{ $data['hora'] }}</p>
                    </li>
                    <li style="color:#000 !important;">
                      <strong>Entrada</strong>
                      <p> {{ $data['entrada'] }}</p>
                    </li>
                    <li style="color:#000 !important;">
                      <strong>Reservación</strong>
                      <p>  {{ $data['reservacion'] }}</p>
                    </li>
                    <li style="color:#000 !important;">
                      <strong>DNI Registrados</strong>
                      <p>  {{ $data['amigo1'] }}</p>
                      <p>  {{ $data['amigo2'] }}</p>
                      <p>  {{ $data['amigo3'] }}</p>
                      <p>  {{ $data['amigo4'] }}</p>
                      <p>  {{ $data['amigo5'] }}</p>
                      <p>  {{ $data['amigo6'] }}</p>
                      <p>  {{ $data['amigo7'] }}</p>
                      <p>  {{ $data['amigo8'] }}</p>
                      <p>  {{ $data['amigo9'] }}</p>
                      <p>  {{ $data['amigo10'] }}</p>

                    </li>
                    <?php if($data['metodo'] == 'efectivo'){ ?>
                      <li style="color:#000 !important;">
                        <strong>Método de Pago</strong>
                        <p> Efectivo</p>
                        <p> Recuerde que debe cancelar el total de: {{ $data['n_transaccion'] }} € en la puerta, para poder disfrutar del evento.</p>
                      </li>
                     <?php } ?>
                     <?php if($data['metodo'] == 'paypal/card'){ ?>
                     <li style="color:#000 !important;">
                       <strong>Método de Pago</strong>
                       <p> Paypal o Card</p>
                       <p> Número de transacción: {{ $data['n_transaccion'] }}</p>
                     </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;">
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
