<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('storage/'.$data->avatar) }}" alt="" style="
                    text-align:center;
                    width: 20vh;
                    height: 20vh;
                    border-radius: 100%;
                    margin-top: 20px;">
                  </span>
                </p>
                <h3 style="color:#fff"><span style="text-transform: capitalize; color:#ffff; font-size:15px;">{{ $data->name }} {{ $data->surnames }}</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 18px;
                                        margin-top: -20px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
                Es un nuevo usuario que requiere de tu aprobación.
              </p>
              </div>
              <div class="body">
                <div class="" style="padding-right: 25px;
                                     display: inline-table;">
                  <p style="font-size:18px; font-weight: bold;">Datos del usuario cliente:</p>
                  <ul style="list-style: none;
                      text-align: justify;
                      font-size:16px;
                      line-height:20px;">
                    <li style="
                        margin-bottom:15px;">
                     <strong> Nombres: </strong> {{ $data->name }} {{ $data->surnames }}
                    </li>
                    <li style="
                        margin-bottom:15px;
                        color: #000!important;">
                     <strong> Email: </strong> {{ $data->email }}
                    </li>
                    <li style="
                        margin-bottom:15px;
                        color: #000!important;">
                      <strong>Fecha de Nacimiento: </strong> {{ $data->date }}
                    </li>
                    <li style="
                        margin-bottom:15px;
                        color: #000!important;">
                      <strong>Sexo: </strong>
                      <?php

                        if($data->sexo == 'f'){
                          $sexo = 'Femenino';
                        }else{
                          $sexo = 'Masculino';
                        }

                       ?>
                       {{ $sexo }}
                    </li>
                    <li style="
                        margin-bottom:15px;">
                      <strong>Fecha de de registro: </strong>{{ $data->created_at }}
                    </li>
                  </ul>

                  <p style="margin-bottom:20px; margin-left:20px; font-size:20px; color: #e73569 !important;"> <a href="{{ url('ap/'.$data->id) }}" target="_blank">Click aquí para aprobar su registro.</a> </p>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
