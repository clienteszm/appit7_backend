<link rel="stylesheet" href="{{ url('css/app.css') }}">
<title>It7 Lounge Club</title>
<link rel="icon" href="{{ asset('images/favicon.ico') }}"  type="image/x-icon"/>
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('storage/users/perfil.jpg') }}" alt="" style="text-align:center; width: 20vh;
                    height: 20vh;
                    border-radius: 100%;
                    margin-top: 20px;">
                  </span>
                </p>
                <h3 style="color:#fff; font-size: 16px;"><span style="text-transform: capitalize; color:#ffff">Ziu a. macayo</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 18px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
              Ya es un usuario aprobado.
              </p>
              </div>


              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center!important;
                                          align-content: center!important;
                                          display: grid!important;
                                          text-align: center!important;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
