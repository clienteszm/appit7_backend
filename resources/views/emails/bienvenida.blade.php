<link rel="stylesheet" href="{{ url('css/app.css') }}">
<div class="header__top" id="barra">
  <div class="container">
    <div class="row">
      <div class="col-md-12 d-flex justify-content-center" data-aos="zoom-in-up">

      </div>
    </div>
  </div>
</div>
<section class="ofrenda__contenido  m-top" style=" padding: 20px 0;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="ofrenda__contenido__intro2" style=" background: #FFFFFF;
                                                        border: 1px solid #EBF0F0;
                                                        box-sizing: border-box;
                                                        padding: 30px 30px 30px 30px;
                                                        box-shadow: 0px 4px 27px rgba(1, 15, 38, 0.04);
                                                        text-align:center;">
          <div class="row">
            <div class="col-lg-6 offset-lg-1">
              <div class="ofrenda__contenido__intro2__p3" style="background-color: #000">
                <p style="  font-size: 30px;
                            line-height: 2.35em;
                            font-family: 'Work+Sans:200', 'work sans', sans-serif;
                            color: #546375;
                            ">
                  <span>
                    <img src="{{ url('images/logo.png') }}" alt="" style="text-align:center; width: 10%;">
                  </span>
                </p>
                <h3 style="color:#fff">Hola <span style="text-transform: capitalize; color:#ffff">{{ $data->name }} {{ $surnames }}</span></h3>
                <p class="split" style="color: #e73569;
                                        font-size: 20px;
                                        font-weight: bold;
                                        padding-bottom:20px;">
                ¡Bienvenido a IT7!
              </p>
              </div>
              <div class="body">

                <div class="" style="padding-right: 25px;
                                     display: inline-table;">
                  <p style="font-size:20px; font-weight: bold;">Ventajas de tener una cuenta</p>
                  <ul style="list-style: none;
                      text-align: justify;">
                    <li>
                     <strong> Compra más rápido</strong>
                     <br>
                     <p>Guarda tus datos para realizar
                       tus compras más rapidamente
                       y evitar pasos extra.</p>
                    </li>
                    <li>
                      <strong>E-Tickets</strong>
                      <p> Con tu ticket digital y codigo QR
                      Entras seguro.</p>
                    </li>
                    <li>
                      <strong>Noticias y ofertas exclusivas</strong>
                      <p>Recibes las últimas noticias y las ofertas especiales con nosotros.</p>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="footer"  style="background-color: #000;
                                          height: 8vh;
                                          align-items: center;
                                          display: grid;
                                          text-align: center;
                                          padding: 10px;" >
                <p style="color: #fff;">© 2020 IT7 Lounge Club, Todos los derechos reservados.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
