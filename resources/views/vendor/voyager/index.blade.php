@extends('voyager::master')

@section('content')
    <div class="page-content">

        <div class="analytics-container">
            <div class="Dashboard Dashboard--full">
                <header class="Dashboard-header">
                    <ul class="FlexGrid">
                        <li class="FlexGrid-item">
                            <div class="Titles" style="align-items:center; align-content: center; display: grid;
                            text-align:center">
                                <h1 class="Titles-main" >Bienvenido al administrador de IT7 Lounge Club</h1>
                            </div>
                        </li>

                    </ul>
                </header>

            </div>
        </div>
    </div>
@stop
