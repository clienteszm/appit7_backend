-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 25-11-2020 a las 12:06:27
-- Versión del servidor: 10.3.25-MariaDB-log-cll-lve
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pbjtgszc_appit7`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `email`, `sexo`, `date`, `puntos`, `avatar`, `estado`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'FedericoAlexis López jimenez', 'lucioo3737@gmail.com', 'm', '1993-04-07', NULL, 'users/user_avatar1605304760.png', 1, 37, '2020-11-14 02:59:21', '2020-11-14 05:34:01'),
(2, 'ELIOENAI Pérez Hernández', 'elioph92@gmail.com', 'm', '1992-05-19', NULL, 'users/user_avatar1605393521.png', 1, 38, '2020-11-15 03:38:41', '2020-11-15 20:59:50'),
(3, 'rayco rodriguez vargas', 'raycorodriguezvargas91@hotmail.com', 'm', '1991-10-29', NULL, 'users/user_avatar1605459696.png', 1, 39, '2020-11-15 21:00:20', '2020-11-15 22:01:36'),
(4, 'Gustavo Henriquez', 'h4dezgh@gmail.com', 'm', '1988-05-21', NULL, 'users/user_avatar1605456640.png', 1, 40, '2020-11-15 21:10:40', '2020-11-15 21:38:23'),
(5, 'José Guillermo Rodríguez Escudero', 'jrodriguezescudero@bintercanarias.com', 'm', '1963-07-14', NULL, 'users/user_avatar1605710181.png', 1, 41, '2020-11-18 19:36:21', '2020-11-18 22:42:48'),
(6, 'prueba Ziuling', 'zmacayo@gmail.com', 'f', '1993-12-26', NULL, 'users/user_avatar1605724917.png', 1, 42, '2020-11-18 22:25:46', '2020-11-18 23:41:57'),
(7, 'kdkdk nnnnn', 'nfn@jks.com', 'f', '2002-09-19', NULL, 'users/user_avatar1605817739.png', 1, 43, '2020-11-20 01:29:00', '2020-11-20 01:30:24'),
(8, 'Tristan Perez', 'perezguzmantristan@gmail.com', 'm', '1981-01-20', NULL, 'users/user_avatar1605870551.png', 1, 44, '2020-11-20 16:09:11', '2020-11-20 16:42:38'),
(9, 'Ruben Dario Cruz Morales', 'boby.chalton.05@gmail.com', 'm', '1986-10-05', NULL, 'users/user_avatar1605872506.png', 1, 45, '2020-11-20 16:41:46', '2020-11-20 16:42:28'),
(10, 'Francisco Javier Sánchez Rodríguez', 'fran_nito_27@hotmail.com', 'm', '1997-10-27', NULL, 'users/user_avatar1605919398.png', 1, 46, '2020-11-21 05:43:18', '2020-11-21 05:43:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `asunto` text NOT NULL,
  `mensaje` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `asunto` text DEFAULT NULL,
  `mensaje` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'email', 'text', 'Email', 0, 0, 1, 1, 1, 1, '{}', 3),
(25, 4, 'sexo', 'select_dropdown', 'Sexo', 0, 0, 1, 1, 1, 1, '{\"default\":\"f\",\"options\":{\"f\":\"Femenino\",\"m\":\"Masculino\"}}', 4),
(26, 4, 'date', 'date', 'Fecha de nacimiento', 0, 1, 1, 1, 1, 1, '{}', 5),
(27, 4, 'puntos', 'text', 'Puntos acumulados', 0, 0, 1, 1, 1, 1, '{}', 6),
(28, 4, 'avatar', 'image', 'Foto de perfil', 0, 1, 1, 1, 1, 1, '{}', 7),
(29, 4, 'estado', 'select_dropdown', 'Estado', 1, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"Registro no aprobado\",\"1\":\"Registro aprobado\"}}', 8),
(30, 4, 'user_id', 'text', 'User Id', 0, 0, 0, 0, 0, 0, '{}', 9),
(31, 4, 'created_at', 'timestamp', 'Fecha de registro', 0, 1, 1, 0, 0, 0, '{}', 10),
(32, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(33, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(34, 5, 'nombre', 'text', 'Nombre', 0, 1, 1, 1, 1, 1, '{}', 2),
(35, 5, 'descripcion', 'text_area', 'Descripción', 0, 0, 1, 1, 1, 1, '{}', 3),
(36, 5, 'fecha', 'date', 'Fecha', 0, 1, 1, 1, 1, 1, '{}', 4),
(37, 5, 'hora_inicio', 'text', 'Hora Inicio', 0, 0, 1, 1, 1, 1, '{}', 5),
(38, 5, 'hora_final', 'text', 'Hora Final', 0, 0, 1, 1, 1, 1, '{}', 6),
(39, 5, 'imagen', 'image', 'Imagen 1080 x 322', 0, 1, 1, 1, 1, 1, '{}', 7),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 8),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(42, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 6, 'titulo', 'text', 'Título', 0, 1, 1, 1, 1, 1, '{}', 2),
(44, 6, 'descripcion', 'text_area', 'Descripción', 0, 1, 1, 1, 1, 1, '{}', 3),
(45, 6, 'precio', 'text', 'Precio', 0, 1, 1, 1, 1, 1, '{}', 4),
(46, 6, 'tipo', 'select_dropdown', 'Tipo', 1, 1, 1, 1, 1, 1, '{\"default\":\"normal\",\"options\":{\"normal\":\"Normal\",\"vip\":\"VIP\"}}', 5),
(47, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(48, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(49, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 7, 'porcentaje', 'text', 'Porcentaje', 0, 1, 1, 1, 1, 1, '{}', 2),
(51, 7, 'cantidad', 'text', 'Cantidad', 0, 1, 1, 1, 1, 1, '{}', 3),
(52, 7, 'estado', 'select_dropdown', 'Estado', 1, 1, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"No disponible\",\"1\":\"Disponible\"}}', 4),
(53, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(54, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(55, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(56, 8, 'cliente_id', 'text', 'Cliente Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(57, 8, 'evento_id', 'text', 'Evento Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(58, 8, 'entrada_id', 'text', 'Entrada Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(59, 8, 'codigo', 'text', 'Codigo', 0, 0, 0, 0, 0, 0, '{}', 5),
(60, 8, 'amigos', 'text', 'Amigos', 0, 0, 1, 1, 1, 1, '{}', 6),
(61, 8, 'metodo', 'text', 'Método', 0, 1, 1, 1, 1, 1, '{}', 7),
(62, 8, 'n_transaccion', 'text', 'N Transaccion / Monto Efectivo', 0, 1, 1, 1, 1, 1, '{}', 8),
(63, 8, 'pago', 'text', 'Pago', 1, 1, 1, 1, 1, 1, '{}', 9),
(64, 8, 'created_at', 'timestamp', 'Fecha', 0, 0, 1, 1, 0, 1, '{}', 10),
(65, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(66, 8, 'venta_belongsto_cliente_relationship', 'relationship', 'Cliente', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Cliente\",\"table\":\"clientes\",\"type\":\"belongsTo\",\"column\":\"cliente_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"clientes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(67, 8, 'venta_belongsto_event_relationship', 'relationship', 'Evento', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Event\",\"table\":\"events\",\"type\":\"belongsTo\",\"column\":\"evento_id\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"clientes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(68, 8, 'venta_belongsto_entrada_relationship', 'relationship', 'Entrada', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Entrada\",\"table\":\"entradas\",\"type\":\"belongsTo\",\"column\":\"entrada_id\",\"key\":\"id\",\"label\":\"titulo\",\"pivot_table\":\"clientes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42'),
(4, 'clientes', 'clientes', 'Cliente', 'Clientes', NULL, 'App\\Cliente', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-07-27 17:00:48', '2020-07-27 17:02:43'),
(5, 'events', 'eventos', 'Event', 'Eventos', NULL, 'App\\Event', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-07-27 17:09:51', '2020-07-27 17:11:08'),
(6, 'entradas', 'entradas', 'Entrada', 'Entradas', NULL, 'App\\Entrada', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(7, 'puntos', 'puntos', 'Punto (Descuento)', 'Puntos (Descuentos)', NULL, 'App\\Punto', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-08-05 16:55:00', '2020-08-05 17:03:08'),
(8, 'ventas', 'ventas', 'Venta', 'Ventas', NULL, 'App\\Venta', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-08-05 17:21:32', '2020-08-05 17:24:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `titulo`, `descripcion`, `precio`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Normal', '(Incluye 1 Consumición)', '10.00', 'normal', NULL, '2020-11-20 22:14:46'),
(2, 'Reservado VIP', 'Acceso para grupo de 5 a 10 personas.\r\n\r\n(Incluye 1 o 2 Botellas de primeras marcas + 10 Refrescos)', '200.00', 'vip', NULL, '2020-11-20 22:15:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora_inicio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hora_final` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `nombre`, `descripcion`, `fecha`, `hora_inicio`, `hora_final`, `imagen`, `created_at`, `updated_at`) VALUES
(1, 'Gran Inauguración', 'Jueves 10 Diciembre será el día elegido para nuestra gran Inauguración.\r\n\r\nDisfrutarás de nuestros mejores cócteles en un ambiente único y especial. \r\n\r\nTe Esperamos!', '2020-12-10', '18:00', '1:00', NULL, '2020-11-15 21:53:00', '2020-11-15 21:56:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-30 12:54:43', '2020-06-30 12:54:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-06-30 12:54:43', '2020-06-30 12:54:43', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-06-30 12:54:45', '2020-06-30 12:54:45', 'voyager.hooks', NULL),
(12, 1, 'Clientes', '', '_self', NULL, NULL, NULL, 15, '2020-07-27 17:00:49', '2020-07-27 17:00:49', 'voyager.clientes.index', NULL),
(13, 1, 'Eventos', '', '_self', NULL, NULL, NULL, 16, '2020-07-27 17:09:51', '2020-07-27 17:09:51', 'voyager.eventos.index', NULL),
(14, 1, 'Entradas', '', '_self', NULL, NULL, NULL, 17, '2020-07-27 17:12:38', '2020-07-27 17:12:38', 'voyager.entradas.index', NULL),
(15, 1, 'Puntos (Descuentos)', '', '_self', NULL, NULL, NULL, 18, '2020-08-05 16:55:00', '2020-08-05 16:55:00', 'voyager.puntos.index', NULL),
(16, 1, 'Ventas', '', '_self', NULL, NULL, NULL, 19, '2020-08-05 17:21:32', '2020-08-05 17:21:32', 'voyager.ventas.index', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_06_29_135152_add_surnames_to_users_table', 2),
(10, '2016_01_01_000000_add_voyager_user_fields', 3),
(11, '2016_01_01_000000_create_data_types_table', 3),
(12, '2016_05_19_173453_create_menu_table', 3),
(13, '2016_10_21_190000_create_roles_table', 3),
(14, '2016_10_21_190000_create_settings_table', 3),
(15, '2016_11_30_135954_create_permission_table', 3),
(16, '2016_11_30_141208_create_permission_role_table', 3),
(17, '2016_12_26_201236_data_types__add__server_side', 3),
(18, '2017_01_13_000000_add_route_to_menu_items_table', 3),
(19, '2017_01_14_005015_create_translations_table', 3),
(20, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 3),
(21, '2017_03_06_000000_add_controller_to_data_types_table', 3),
(22, '2017_04_21_000000_add_order_to_data_rows_table', 3),
(23, '2017_07_05_210000_add_policyname_to_data_types_table', 3),
(24, '2017_08_05_000000_add_group_to_settings_table', 3),
(25, '2017_11_26_013050_add_user_role_relationship', 3),
(26, '2017_11_26_015000_create_user_roles_table', 3),
(27, '2018_03_11_000000_add_user_settings', 3),
(28, '2018_03_14_000000_add_details_to_data_types_table', 3),
(29, '2018_03_16_000000_make_settings_value_nullable', 3),
(30, '2020_07_07_175109_add_statu_to_users_table', 4),
(40, '2020_07_07_175132_create_clientes_table', 5),
(41, '2020_07_07_175329_create_events_table', 5),
(42, '2020_07_07_175514_create_entradas_table', 5),
(43, '2020_07_07_175728_create_stocks_table', 5),
(44, '2020_07_07_175753_create_ventas_table', 5),
(45, '2020_07_14_115705_add_metodo_to_ventas_table', 6),
(46, '2020_07_14_115814_create_puntos_table', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('017a1bf2b9688558bb0c715c698f50724bb64e0209d9a5cfb95ac9f28e442de8eddfb93e28d8d4c2', 28, 1, 'authToken', '[]', 0, '2020-07-24 18:00:09', '2020-07-24 18:00:09', '2021-07-24 14:00:09'),
('02b57ee6cef7cfb91ae8ce88da25148d77f362290bdc9682b7451a708336691b31a4760bf4681e50', 3, 1, 'authToken', '[]', 0, '2020-07-05 17:39:05', '2020-07-05 17:39:05', '2021-07-05 18:39:05'),
('045e60fa2c47d79c8fe6c05d69dd5e7050c3014ee33ba363f3d535db5524cabf5a00ca86b5372b72', 11, 1, 'authToken', '[]', 0, '2020-07-02 17:33:40', '2020-07-02 17:33:40', '2021-07-02 18:33:40'),
('05773e7ab803a42cf7b3dafa699944e1c542d0ef71fc7dfbf039c3fae492c85999d7f51372736c74', 5, 1, 'authToken', '[]', 0, '2020-07-02 14:37:59', '2020-07-02 14:37:59', '2021-07-02 15:37:59'),
('06cd637a11ea51beac5ca41ae4d8f43b0f246a5f214b1b0037458b2acd0646de539a0bffc24f9514', 10, 1, 'authToken', '[]', 0, '2020-07-02 17:23:23', '2020-07-02 17:23:23', '2021-07-02 18:23:23'),
('11278eeb85a5e9d0850989a7af019710fe9f2d0fe33de5c5832918b9c3cbc001cd04336f74a96d43', 46, 1, 'authToken', '[]', 0, '2020-11-21 05:43:18', '2020-11-21 05:43:18', '2021-11-21 00:43:18'),
('137454af64487a38d45f33c786455431c4c902e65cb3326e8df3e44a2d5e6cd3ac6e045d1a663ff3', 38, 1, 'authToken', '[]', 0, '2020-11-15 03:38:41', '2020-11-15 03:38:41', '2021-11-14 22:38:41'),
('147b62e9253592efd74a3778793eb2f0c038a6d3a2f0bf237731062b1ef3638073a57a6451916d03', 10, 1, 'authToken', '[]', 0, '2020-07-23 03:11:09', '2020-07-23 03:11:09', '2021-07-22 23:11:09'),
('15bdf639aab1b3e59f9c2e26b8a00a249ffdfd14c9d2844a1d46d6c74fdd37b702fb458a378ccff4', 30, 1, 'authToken', '[]', 0, '2020-07-26 22:03:59', '2020-07-26 22:03:59', '2021-07-26 18:03:59'),
('1b6015f5143d87c5893b94b4014d4140f8c292c8eba9f9239deac945c7c5c2aadb6c0d75b28e6dce', 42, 1, 'authToken', '[]', 0, '2020-11-18 22:25:46', '2020-11-18 22:25:46', '2021-11-18 17:25:46'),
('1ffcdba064f3028608ab6d84cf43ceadb48331a83e3e067ae3e1339c4ed19de85a08deb61a5d3c29', 43, 1, 'authToken', '[]', 0, '2020-11-20 01:29:00', '2020-11-20 01:29:00', '2021-11-19 20:29:00'),
('240d537f2927c7de63bad41fb93c5d515c5bc7b56dac485df485ff30200f3ec003f10241eda67a41', 5, 1, 'authToken', '[]', 0, '2020-07-02 14:37:57', '2020-07-02 14:37:57', '2021-07-02 15:37:57'),
('24ee13478aacdaf016201ed27f398b23752f47247714415061f2e35a01d9e16febead6b48a74eeb7', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:18:46', '2020-07-10 11:18:46', '2021-07-10 12:18:46'),
('257a2e8e74f56fb0f8dee521ef29e3b77f405468c119a14abf723335ee33988d4e6d28df41bc31cf', 15, 1, 'authToken', '[]', 0, '2020-07-24 01:05:30', '2020-07-24 01:05:30', '2021-07-23 21:05:30'),
('27ef9f259998bb82f5caa8894539de8fa8334a36c19f9be7f59688c0bb5e75c135339fcbe2c2794d', 8, 1, 'authToken', '[]', 0, '2020-07-23 02:29:14', '2020-07-23 02:29:14', '2021-07-22 22:29:14'),
('29b688b041c332d099b58ec7111017d901022804f8fbe28238c71e540f57d939879e32bffbfaf235', 9, 1, 'authToken', '[]', 0, '2020-07-02 17:02:57', '2020-07-02 17:02:57', '2021-07-02 18:02:57'),
('2cf9f2222541a510c2908fd7f2cd8262ec87346e7e57ff35d2c6f24ca6b7156ec140f1ea7416be87', 44, 1, 'authToken', '[]', 0, '2020-11-20 16:09:11', '2020-11-20 16:09:11', '2021-11-20 11:09:11'),
('2dbdb3b5af2cd56a3630aa39eb5229106c0e3cd4a3e41a0e976435d38d1b3f7e18e8757cc8793db6', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:08:20', '2020-07-10 11:08:20', '2021-07-10 12:08:20'),
('30b0c2d9b059a56229b7a67f78a7c1bf77d1a7774fec8750974b9422c95575cb537b3e8bed52ce5d', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:49:53', '2020-07-10 11:49:53', '2021-07-10 12:49:53'),
('32f984e3e489014fb7084ccbf3448a3a4c9c756249ba0f64bcbea930ec7b9b1493429018d062120a', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:46:01', '2020-07-10 11:46:01', '2021-07-10 12:46:01'),
('3922a9d4037f85ebeada692ab92ee169318f5f6314836261067ad36d24bcd7cd85d9954eeac8a430', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:16:44', '2020-07-10 11:16:44', '2021-07-10 12:16:44'),
('3cf236b9394d3f4a6e19adc62cab092f46ec807a2b5d4732c33e2f6d4cc284075eaa20bfdb596534', 41, 1, 'authToken', '[]', 0, '2020-11-18 19:36:21', '2020-11-18 19:36:21', '2021-11-18 14:36:21'),
('3d193653850e29c52d27ff9ce99ca34f8486d0e615f098385abb3ee3aedd83b2b252a29041a267be', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:03:14', '2020-07-02 17:03:14', '2021-07-02 18:03:14'),
('3ff6c91425efc286011ea7da58cb226eb454711641ba7c046bd52a1bd60b6222c3e0dd2a7e824ade', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:18:32', '2020-06-30 20:18:32', '2021-06-30 21:18:32'),
('4224b85471f8cd687d400b5941695cdab51ec4dc61cf059706b0749363f11ebc5e11a2bf5dad6bdf', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:10:19', '2020-06-30 20:10:19', '2021-06-30 21:10:19'),
('4656d7663d05f65da7cf15b7935e57f1f6ac1bc20b6287d251acde8e146a00841f8dfd9bf461a8d0', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:19:16', '2020-06-30 19:19:16', '2021-06-30 20:19:16'),
('55b5a920cbbf65b4160378eb99a97a1d5b516f07a297829fbaf3337c528a396bc839526cce8bda1f', 32, 1, 'authToken', '[]', 0, '2020-08-05 08:51:03', '2020-08-05 08:51:03', '2021-08-05 04:51:03'),
('57a09e486e531ee44e7ebdb9f607ac624de65f6b5358ff8d1589267e7aea88ee3d0ce4a0ea909095', 40, 1, 'authToken', '[]', 0, '2020-11-15 21:10:40', '2020-11-15 21:10:40', '2021-11-15 16:10:40'),
('5a9c9f25638737f215329f0272270401e107d874f886653aebb38ba2b9625b5f56447de49cdb5e0c', 2, 1, 'authToken', '[]', 0, '2020-07-12 09:50:39', '2020-07-12 09:50:39', '2021-07-12 10:50:39'),
('5b9476619ad6dc861883c43299adcc94e4710ec57137f82ffbd2cf89a560cbe6a38200add583fba1', 18, 1, 'authToken', '[]', 0, '2020-07-24 02:30:44', '2020-07-24 02:30:44', '2021-07-23 22:30:44'),
('5e02dc722d2f89b61cb51f3173675676d314ece8f9d3cdc5c8f3e5247fe776ae26ff99bf50b9fdbf', 3, 1, 'authToken', '[]', 0, '2020-07-02 14:07:45', '2020-07-02 14:07:45', '2021-07-02 15:07:45'),
('5e13b68a8fe1055a828b0c92a51bbc48741785c7dced832fb406f1568ab9f393cb0312104f13a202', 3, 1, 'authToken', '[]', 0, '2020-07-10 10:49:32', '2020-07-10 10:49:32', '2021-07-10 11:49:32'),
('5e9229e730ded457068a3cf5a8108ee9f649cee7a07174d7cfd1c59f2d22db6cb9c8a3142b58f732', 3, 1, 'authToken', '[]', 0, '2020-07-10 10:45:25', '2020-07-10 10:45:25', '2021-07-10 11:45:25'),
('5f32ce2d80924a2a4213df01f13d5e0dee144cabb09f6bf6a62f3ca0be7b5d47b54df0f95491348d', 36, 1, 'authToken', '[]', 0, '2020-08-08 17:08:05', '2020-08-08 17:08:05', '2021-08-08 13:08:05'),
('5f9098ae8dd8eca194cd16d422f70e95def5f638b0858a937baf6436eb83883f25f44477dc6728d7', 7, 1, 'authToken', '[]', 0, '2020-07-02 15:37:38', '2020-07-02 15:37:38', '2021-07-02 16:37:38'),
('5f9f4208480e987ac94ad24a00d17a47d96a4d1c61b3365fa7fcf26736aa302ef0c563058ff04f7c', 26, 1, 'authToken', '[]', 0, '2020-07-24 16:16:50', '2020-07-24 16:16:50', '2021-07-24 12:16:50'),
('622599fa151fdd9dedcae4965f9c578270e341aea5414e129a26d9afb396126e2d0912425714d100', 1, 1, 'authToken', '[]', 0, '2020-07-02 16:06:54', '2020-07-02 16:06:54', '2021-07-02 17:06:54'),
('69c666e92faeca5f474115d85c0865c24b0ec09b40e20f98ded3e527c5a5c17fb8a73d90178d2a53', 7, 1, 'authToken', '[]', 0, '2020-07-10 15:21:15', '2020-07-10 15:21:15', '2021-07-10 16:21:15'),
('6a252994fa0527737acae10f64ccc540ebe94ff47755c684ac3aae564261a75a3aa957e5e2f6097e', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:16:40', '2020-06-30 19:16:40', '2021-06-30 20:16:40'),
('6b3ce65c23e1e7c6c10fd136fe2e9a261664dc84133fd1c71be96f066a9a00f0325fa55853ea8e3c', 6, 1, 'authToken', '[]', 0, '2020-07-22 16:23:06', '2020-07-22 16:23:06', '2021-07-22 12:23:06'),
('6c5ab4fc09f21e5330746240f47a1852ffef112657d137e7030479c78352d5ecc9b8b6374991776e', 22, 1, 'authToken', '[]', 0, '2020-07-24 03:52:48', '2020-07-24 03:52:48', '2021-07-23 23:52:48'),
('6e045a6c3980ca65d3d599cd87e5660c72a59cd364669e82554d744d1cc6e081562fae41218d0e0a', 3, 1, 'authToken', '[]', 0, '2020-07-02 14:07:43', '2020-07-02 14:07:43', '2021-07-02 15:07:43'),
('6f671551d1d31b2e1f29cc1ea2e1e2952004f9ae34826789e4bd6aefbe9a4021e41944b9bbac6233', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:04:27', '2020-06-30 20:04:27', '2021-06-30 21:04:27'),
('700103754d4f52440b97b88566f7d1b7dfab50dc8fb2f0900efbe69fec6ed6cff576819e7a86cd94', 19, 1, 'authToken', '[]', 0, '2020-07-24 02:32:52', '2020-07-24 02:32:52', '2021-07-23 22:32:52'),
('72f31fec56db3a4191741aa0851af48f37e5dd41ddc977b82efb6a5ecdf355c95e73992170fdbad7', 1, 1, 'authToken', '[]', 0, '2020-07-02 09:09:31', '2020-07-02 09:09:31', '2021-07-02 10:09:31'),
('7362d0ef79564ad1dfbd93c51f35258abd8397d141af483b9fe45bc39dd1cb80ec34cfb6b9673310', 34, 1, 'authToken', '[]', 0, '2020-08-05 14:41:59', '2020-08-05 14:41:59', '2021-08-05 10:41:59'),
('7951efd8f99868c8ea07e3341b44b678c10c6583b075efec2faa547ae5f5ebbdfdfd7ed6ad9a39c7', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:24:24', '2020-07-10 11:24:24', '2021-07-10 12:24:24'),
('7ad885ba2f3bed124080b05eca51acb5dbec100da6fa63a93fd102a227facb94a0a2558cc3a3bd42', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:01:26', '2020-07-02 17:01:26', '2021-07-02 18:01:26'),
('7b535c36a76ba653efe41fe7f88da4f50257fdc541b2c467e9ebf9f14ef32af61eb5920c5649d134', 11, 1, 'authToken', '[]', 0, '2020-07-23 03:44:37', '2020-07-23 03:44:37', '2021-07-22 23:44:37'),
('7dcbb61d5640db0c7dc2c3b1998861be213948a0348de3bb60a24286da1a29dbdaeca8c58ae8ae5a', 25, 1, 'authToken', '[]', 0, '2020-07-24 16:14:20', '2020-07-24 16:14:20', '2021-07-24 12:14:20'),
('81b3acec521d2676ef376d9336d22062ab474bde59cdf78bd9729972457891bfa140643d67848ccf', 24, 1, 'authToken', '[]', 0, '2020-07-24 15:07:28', '2020-07-24 15:07:28', '2021-07-24 11:07:28'),
('888f33d90ebe583353e20809880bf36027d5c63e9dbec58c2f03e25131404d8e519b4878aafa93a0', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:31:41', '2020-07-10 11:31:41', '2021-07-10 12:31:41'),
('8b13e604d7f311d48d2892f1c2d1bc21fc81ef4d51c5a804daead40ec6c361323432fe6f110619f2', 12, 1, 'authToken', '[]', 0, '2020-07-23 04:05:10', '2020-07-23 04:05:10', '2021-07-23 00:05:10'),
('8b6e034969c624cf3ebda3e0d6943065c165b0add9eb9a825be7174228cb3d19f06ca741746ae172', 35, 1, 'authToken', '[]', 0, '2020-08-05 19:18:27', '2020-08-05 19:18:27', '2021-08-05 15:18:27'),
('8bb6343952c6684290077f630828a391bd220c43070628483e5f30a8aa99e1dc94da650790de34b2', 6, 1, 'authToken', '[]', 0, '2020-07-02 15:33:57', '2020-07-02 15:33:57', '2021-07-02 16:33:57'),
('8da87b08874ea9b9eebafddd8c0dc233e54398737eb76a73f4c1ac1c08824aa17987dddaee9d0d5a', 29, 1, 'authToken', '[]', 0, '2020-07-24 18:56:05', '2020-07-24 18:56:05', '2021-07-24 14:56:05'),
('8dafa436e00ce7df0c31e2cd7ac328ca5f540aa859fea888f71afbb83318ea5d10f5c57761e697fa', 9, 1, 'authToken', '[]', 0, '2020-07-10 15:23:14', '2020-07-10 15:23:14', '2021-07-10 16:23:14'),
('8db939312667adad9af1f2da43fd8526e8e842068e4b204e7e5e266c07d846f8d93063c8f66eb254', 45, 1, 'authToken', '[]', 0, '2020-11-20 16:41:46', '2020-11-20 16:41:46', '2021-11-20 11:41:46'),
('8f85a217347a22de3439937a1c147f4ac488e86e00d367f3af046de19aca967f69f3b4f8de38d574', 7, 1, 'authToken', '[]', 0, '2020-07-02 16:25:29', '2020-07-02 16:25:29', '2021-07-02 17:25:29'),
('9148ec6c101e815af4590a4b47b135c5c008ea07d2fc8f90d1bde947fdde449a86caf59480bcca1f', 14, 1, 'authToken', '[]', 0, '2020-07-24 00:59:17', '2020-07-24 00:59:17', '2021-07-23 20:59:17'),
('95907f667196d5b27b620fdf2cb9860381b57fe9b97da1a5f5ece12099dd7e950f0707f905095ef9', 4, 1, 'authToken', '[]', 0, '2020-07-02 14:28:47', '2020-07-02 14:28:47', '2021-07-02 15:28:47'),
('96d3def359293efb128109dbba35ee0d07c975b8023146eeea932f6640f9f05bd43fc04250733119', 6, 1, 'authToken', '[]', 0, '2020-07-02 15:33:55', '2020-07-02 15:33:55', '2021-07-02 16:33:55'),
('9bf1982e7d1b53c75ec0b7a236cb7f3c96ce476f154eeec0ce6d44d6f5b2a22e683002e57c52437a', 20, 1, 'authToken', '[]', 0, '2020-07-24 03:06:32', '2020-07-24 03:06:32', '2021-07-23 23:06:32'),
('9e97e466d647ed71d1f94b571d40eb77cad431b8566e9b37d34bad6c60d4af8e6162595311198711', 7, 1, 'authToken', '[]', 0, '2020-07-02 16:18:04', '2020-07-02 16:18:04', '2021-07-02 17:18:04'),
('a089a774fb3160d43dc5889d4b72b94fac4c93d272ceac3253363273a7f64b2b46ba9858ee8f0a1f', 4, 1, 'authToken', '[]', 0, '2020-07-21 19:10:12', '2020-07-21 19:10:12', '2021-07-21 15:10:12'),
('a1986673d6b3c6f5fa88c70fd739b62d477b3faef0861587af330007380a0ea0c249a6d789ca1877', 10, 1, 'authToken', '[]', 0, '2020-07-02 17:23:25', '2020-07-02 17:23:25', '2021-07-02 18:23:25'),
('a245903551072ce39d040fe509a1132ebe335b0088dd9443bc87a8793375d04462876fd91a08611c', 4, 1, 'authToken', '[]', 0, '2020-07-02 14:28:54', '2020-07-02 14:28:54', '2021-07-02 15:28:54'),
('a5dfa6e8f1fc18b1a226e676a8daaf9d7c81f874ccbae61af246663a82852fc6ddf0648bda914963', 7, 1, 'authToken', '[]', 0, '2020-07-23 02:26:50', '2020-07-23 02:26:50', '2021-07-22 22:26:50'),
('a7a71c33e05950769db83ac46b01189ba543dfb20e15060ba416cb892fea7fa15be4a7df0c94e7c4', 37, 1, 'authToken', '[]', 0, '2020-11-14 02:59:21', '2020-11-14 02:59:21', '2021-11-13 21:59:21'),
('a7f8c0644a425a23da529d6e8cd2181418ec0456da9836d06b151021171f6bf2b602a9ced169421b', 21, 1, 'authToken', '[]', 0, '2020-07-24 03:28:28', '2020-07-24 03:28:28', '2021-07-23 23:28:28'),
('abaeecb7f35efdc740feb5a2f36394e707639b37df218265d8f6c2a803ccb0e39d62a9cf086c088b', 12, 1, 'authToken', '[]', 0, '2020-07-02 17:34:59', '2020-07-02 17:34:59', '2021-07-02 18:34:59'),
('add947db0ebe10e5bc3b0d530ff68efeca4c968daef603e1ab9fad2d8597a00a7d2fdc31f31ddaea', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:18:18', '2020-06-30 20:18:18', '2021-06-30 21:18:18'),
('b2981818310ea2f0d6eeab0177be11542146065a27c6b5ff4e6bfb030a5e6cd7ab171e513838aebe', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:31:49', '2020-06-30 19:31:49', '2021-06-30 20:31:49'),
('b3113c44325d8793eda5777b240a31850b9a93d7197abbe1e1f515f0415e109b6ecc6d6538603299', 17, 1, 'authToken', '[]', 0, '2020-07-24 02:25:46', '2020-07-24 02:25:46', '2021-07-23 22:25:46'),
('b321c4195a8723ae41eefa991523a296e392eaaa81719281573956660ca95a937e8bd37728107dcc', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:00:35', '2020-07-02 17:00:35', '2021-07-02 18:00:35'),
('b73c9a42b4e35d100cfd245195cca3f5ca21ede3244af5686b5647dfce48bf9c1bba99123e06c3cd', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:14:29', '2020-07-10 11:14:29', '2021-07-10 12:14:29'),
('b89bde0f49ca65bae4045f93f970fd8bfa7820eb285de1d000689255ee88b9922f5e867049de2284', 6, 1, 'authToken', '[]', 0, '2020-07-10 15:07:12', '2020-07-10 15:07:12', '2021-07-10 16:07:12'),
('b90bec7ff97eca925fb4c4be7f094862eab32a914df0918a188513ea68ca2fbb90cec37c274bc0a2', 23, 1, 'authToken', '[]', 0, '2020-07-24 04:05:37', '2020-07-24 04:05:37', '2021-07-24 00:05:37'),
('c0f2ce8ccd601e0c99a235462ac520b01aebcc09ac13ff2dcb76fcb4587c75d657a0c1e28787590d', 27, 1, 'authToken', '[]', 0, '2020-07-24 16:17:32', '2020-07-24 16:17:32', '2021-07-24 12:17:32'),
('c1298b7490359bd18e521c1ce25e463964c57a994f516c1b9bb962d0c1bd125af355994843d87975', 3, 1, 'authToken', '[]', 0, '2020-07-03 18:11:31', '2020-07-03 18:11:31', '2021-07-03 19:11:31'),
('c1b93c2547428aafbb1dc5cca0d79b47c24038c1a6f70f88636855e811c5c539ab63e3e0b21fa585', 13, 1, 'authToken', '[]', 0, '2020-07-23 04:11:47', '2020-07-23 04:11:47', '2021-07-23 00:11:47'),
('c1f79e83b17a921a238e9c75fdb6dc9be6251bf044161e24e003b6b479c37809f92b045767e0f2b8', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:46:58', '2020-06-30 19:46:58', '2021-06-30 20:46:58'),
('c5500de172f950ed1751ad03d9b1abae19be1311807ecc33cde4efd38cf63f8b748b7ddb74e91ad4', 16, 1, 'authToken', '[]', 0, '2020-07-24 01:40:50', '2020-07-24 01:40:50', '2021-07-23 21:40:50'),
('c83ccf747a96d3396ae1c51bb39fa3cdeee38f508c7303ead5dbdc18adb6ebb802a688223ead1333', 4, 1, 'authToken', '[]', 0, '2020-07-10 15:00:19', '2020-07-10 15:00:19', '2021-07-10 16:00:19'),
('c86a59f92993e2fdbbd7f7eb833aff7722d387de90bee3a245969c3f1175e021f72d7ff2ec48b73e', 3, 1, 'authToken', '[]', 0, '2020-07-05 17:39:33', '2020-07-05 17:39:33', '2021-07-05 18:39:33'),
('c878091b2fbcc863dfd0275f0e9e1e61a1d4039bb3300ea6b49b4e1da88c52d7befbcb5cbb67b4ef', 9, 1, 'authToken', '[]', 0, '2020-07-02 17:02:59', '2020-07-02 17:02:59', '2021-07-02 18:02:59'),
('d551256971e83379277ea9d67f6202c8a124328354ac6c0557f0b49ec4e7757ba41198fabce3c727', 5, 1, 'authToken', '[]', 0, '2020-07-22 16:19:31', '2020-07-22 16:19:31', '2021-07-22 12:19:31'),
('d6a609eb5cd172d17b4a952bf84feb15ebecd5170f0c8e15c565387bc4faa4c378083a85d5a8121a', 8, 1, 'authToken', '[]', 0, '2020-07-10 15:22:41', '2020-07-10 15:22:41', '2021-07-10 16:22:41'),
('d8f64cfb573c6e3601a77fc8356633ba2e23124bdf14ad32a46747006fb61d503aaec9f3f5b1bb63', 9, 1, 'authToken', '[]', 0, '2020-07-23 02:39:43', '2020-07-23 02:39:43', '2021-07-22 22:39:43'),
('d9e475ea408c954d240ec112dbe1ef8ac9a1420f54cff9a18648f99698cecee00beca948abf254ce', 39, 1, 'authToken', '[]', 0, '2020-11-15 21:00:20', '2020-11-15 21:00:20', '2021-11-15 16:00:20'),
('db8eedac55f381c0a8b70b2f4f7bced656480883319a72c7b65010f919f27942d3213733678b41de', 12, 1, 'authToken', '[]', 0, '2020-07-02 17:34:57', '2020-07-02 17:34:57', '2021-07-02 18:34:57'),
('dff9d89237c55096b3b544370ffc812014224d65bda337aa9c95c91344b489259acf571d24c5f871', 11, 1, 'authToken', '[]', 0, '2020-07-02 17:33:49', '2020-07-02 17:33:49', '2021-07-02 18:33:49'),
('e1dd3332dc8f585909454a644e5d725ef84ac985e4e9a3e54b4c5a78bb46ac411700f4e46fed3325', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:00:38', '2020-07-02 17:00:38', '2021-07-02 18:00:38'),
('e4434ff51d461c23022d82c8668e618735c02ad5592fd57b70b53ec4b9e80d3fccc8fa277fc02168', 3, 1, 'authToken', '[]', 0, '2020-07-05 16:32:17', '2020-07-05 16:32:17', '2021-07-05 17:32:17'),
('e776cb1fa06ac96f343395e6dc5c3f7d7dd24228291c7e13e13bbf659344eba6d73161ae96a00b13', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:09:50', '2020-07-10 11:09:50', '2021-07-10 12:09:50'),
('e78585bfdcbba7a5757b67dc15c906859d49695e7964ceb30a7d07269e4d2109a4acf5bbd1f6eda2', 5, 1, 'authToken', '[]', 0, '2020-07-10 15:05:15', '2020-07-10 15:05:15', '2021-07-10 16:05:15'),
('eb4b2d23e73b8c44518eacf692eca9769ddd41605988657e7a984ef9ab1dc097df1b97f63a6386b3', 33, 1, 'authToken', '[]', 0, '2020-08-05 13:46:15', '2020-08-05 13:46:15', '2021-08-05 09:46:15'),
('f12db7947770d22ab33fa12a83ccfcaea6f38eb839b31d30e27eb76eca576e0ac60ab49fa78c2541', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:25:54', '2020-07-10 11:25:54', '2021-07-10 12:25:54'),
('f4787343373dc12ad9fefc2d460646ff73a4821b0f4e9c7c376bef50350f2aa7d980d26b66687091', 7, 1, 'authToken', '[]', 0, '2020-07-02 15:37:37', '2020-07-02 15:37:37', '2021-07-02 16:37:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '2XtkPqrBS5ZBF4cYkx2DsY6V2AOXZamyuTqxz8Ss', NULL, 'http://localhost', 1, 0, 0, '2020-06-29 12:15:35', '2020-06-29 12:15:35'),
(2, NULL, 'Laravel Password Grant Client', 'sHLRE2DcbP0GWhWvw1sthL8ijWHJGqkeW5HliS0a', 'users', 'http://localhost', 0, 1, 0, '2020-06-29 12:15:35', '2020-06-29 12:15:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-29 12:15:35', '2020-06-29 12:15:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(2, 'browse_bread', NULL, '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(3, 'browse_database', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(4, 'browse_media', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(5, 'browse_compass', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(6, 'browse_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(7, 'read_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(8, 'edit_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(9, 'add_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(10, 'delete_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(11, 'browse_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(12, 'read_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(13, 'edit_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(14, 'add_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(15, 'delete_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(16, 'browse_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(17, 'read_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(18, 'edit_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(19, 'add_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(20, 'delete_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(21, 'browse_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(22, 'read_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(23, 'edit_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(24, 'add_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(25, 'delete_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(26, 'browse_hooks', NULL, '2020-06-30 12:54:45', '2020-06-30 12:54:45'),
(27, 'browse_clientes', 'clientes', '2020-07-27 17:00:49', '2020-07-27 17:00:49'),
(28, 'read_clientes', 'clientes', '2020-07-27 17:00:49', '2020-07-27 17:00:49'),
(29, 'edit_clientes', 'clientes', '2020-07-27 17:00:49', '2020-07-27 17:00:49'),
(30, 'add_clientes', 'clientes', '2020-07-27 17:00:49', '2020-07-27 17:00:49'),
(31, 'delete_clientes', 'clientes', '2020-07-27 17:00:49', '2020-07-27 17:00:49'),
(32, 'browse_events', 'events', '2020-07-27 17:09:51', '2020-07-27 17:09:51'),
(33, 'read_events', 'events', '2020-07-27 17:09:51', '2020-07-27 17:09:51'),
(34, 'edit_events', 'events', '2020-07-27 17:09:51', '2020-07-27 17:09:51'),
(35, 'add_events', 'events', '2020-07-27 17:09:51', '2020-07-27 17:09:51'),
(36, 'delete_events', 'events', '2020-07-27 17:09:51', '2020-07-27 17:09:51'),
(37, 'browse_entradas', 'entradas', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(38, 'read_entradas', 'entradas', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(39, 'edit_entradas', 'entradas', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(40, 'add_entradas', 'entradas', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(41, 'delete_entradas', 'entradas', '2020-07-27 17:12:38', '2020-07-27 17:12:38'),
(42, 'browse_puntos', 'puntos', '2020-08-05 16:55:00', '2020-08-05 16:55:00'),
(43, 'read_puntos', 'puntos', '2020-08-05 16:55:00', '2020-08-05 16:55:00'),
(44, 'edit_puntos', 'puntos', '2020-08-05 16:55:00', '2020-08-05 16:55:00'),
(45, 'add_puntos', 'puntos', '2020-08-05 16:55:00', '2020-08-05 16:55:00'),
(46, 'delete_puntos', 'puntos', '2020-08-05 16:55:00', '2020-08-05 16:55:00'),
(47, 'browse_ventas', 'ventas', '2020-08-05 17:21:32', '2020-08-05 17:21:32'),
(48, 'read_ventas', 'ventas', '2020-08-05 17:21:32', '2020-08-05 17:21:32'),
(49, 'edit_ventas', 'ventas', '2020-08-05 17:21:32', '2020-08-05 17:21:32'),
(50, 'add_ventas', 'ventas', '2020-08-05 17:21:32', '2020-08-05 17:21:32'),
(51, 'delete_ventas', 'ventas', '2020-08-05 17:21:32', '2020-08-05 17:21:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(18, 2),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(31, 1),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(41, 1),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(48, 1),
(48, 2),
(49, 1),
(50, 1),
(51, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos`
--

CREATE TABLE `puntos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `porcentaje` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `puntos`
--

INSERT INTO `puntos` (`id`, `porcentaje`, `cantidad`, `estado`, `created_at`, `updated_at`) VALUES
(1, '5', 100, 1, NULL, '2020-08-13 00:40:20'),
(2, '10', 200, 1, NULL, '2020-08-13 00:40:38'),
(3, '15', 300, 1, NULL, '2020-08-13 00:41:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(2, 'user', 'Normal User', '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(3, 'usuario', 'Usuario App', NULL, NULL),
(4, 'lector', 'Lector QR', '2020-08-04 18:05:51', '2020-08-04 18:06:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/July2020/J7ZhbN7WspjTxeoqtl7q.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'It7 Lounge Club', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Bienvenido al administrador de IT7 Lounge Club', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings/July2020/gBblBzs49yKuy7GWlSJF.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/July2020/EtYkJS87UXhGdXYkC9Ew.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `evento_id` int(10) UNSIGNED DEFAULT NULL,
  `entrada_id` int(10) UNSIGNED DEFAULT NULL,
  `limite` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surnames` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `puntos` int(11) NOT NULL DEFAULT 0,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `surnames`, `sexo`, `date`, `puntos`, `estado`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', NULL, NULL, NULL, 0, 0, 'admin@appit7.com', 'users/default.png', NULL, '$2y$10$Qc/QalnlGIRI2uKA.au13Ojwz9zZPJ11ylWBKPDAte6j4ZeWAce7a', NULL, NULL, '2020-06-30 13:03:36', '2020-06-30 13:03:36'),
(2, 3, 'ziu', 'macayo lópez', 'f', '2002-07-12', 0, 1, 'zmacayo2@gmail.com', 'users/user_avatar1595786551.png', NULL, '$2y$10$jqlaFMnlTMTp80C9dF.YpekygyZ6d8wB51CCbp6djwl.puWPoZaxW', NULL, NULL, '2020-07-12 09:50:39', '2020-07-26 22:02:31'),
(3, 2, 'user', NULL, NULL, NULL, 0, 1, 'user@appit7.com', 'users/default.png', NULL, '$2y$10$HLg92AHseJ74SkKn8et5p.nStuxQi68UO4xz9zFmlkf.olGIsSawa', NULL, NULL, '2020-07-17 09:43:02', '2020-07-17 09:43:02'),
(37, 3, 'FedericoAlexis', 'López jimenez', 'm', '1993-04-07', 0, 1, 'lucioo3737@gmail.com', 'users/user_avatar1605304760.png', NULL, '$2y$10$JnZvN4Pxqp4lQSBeUsrI8u7Kht5nrrjLDTRl.uRLajCMRoLLIXJiC', NULL, NULL, '2020-11-14 02:59:21', '2020-11-14 05:34:01'),
(38, 3, 'ELIOENAI', 'Pérez Hernández', 'm', '1992-05-19', 0, 1, 'elioph92@gmail.com', 'users/user_avatar1605393521.png', NULL, '$2y$10$vNcm0hDO.sMsw2IWBTskJ.uMgsC23PF1Dmh5mh0RQdkoLka3j3slC', NULL, NULL, '2020-11-15 03:38:41', '2020-11-15 20:59:50'),
(39, 3, 'rayco', 'rodriguez vargas', 'm', '1991-10-29', 1, 1, 'raycorodriguezvargas91@hotmail.com', 'users/user_avatar1605459696.png', NULL, '$2y$10$k.b.uv9MuDn81r6dclZYTOrbtjFqhp12ItKR/PWMa4nIExFcfaO7i', NULL, NULL, '2020-11-15 21:00:20', '2020-11-15 22:01:36'),
(40, 3, 'Gustavo', 'Henriquez', 'm', '1988-05-21', 1, 1, 'h4dezgh@gmail.com', 'users/user_avatar1605456640.png', NULL, '$2y$10$KO/l3ou96gtkNtisdgSfve2ikF75AjmJ6/TmFaUGvIQkwAvePezYy', NULL, NULL, '2020-11-15 21:10:40', '2020-11-22 02:03:24'),
(41, 3, 'José Guillermo', 'Rodríguez Escudero', 'm', '1963-07-14', 0, 1, 'jrodriguezescudero@bintercanarias.com', 'users/user_avatar1605710181.png', NULL, '$2y$10$mOAtUzdc5A6pbi9NYWCMfOcsYKuPfIYpON47lY9ce692FFWyuAgZK', NULL, NULL, '2020-11-18 19:36:21', '2020-11-18 22:42:48'),
(42, 3, 'prueba', 'Ziuling', 'f', '1993-12-26', 1, 1, 'zmacayo@gmail.com', 'users/user_avatar1605724917.png', NULL, '$2y$10$Xhf4ZioK.chzcsl4Pvcxu.Nv4Awz2o4Ea952VLGyw4Zw7bzGYnd8y', NULL, NULL, '2020-11-18 22:25:46', '2020-11-19 00:55:07'),
(43, 3, 'kdkdk', 'nnnnn', 'f', '2002-09-19', 0, 1, 'nfn@jks.com', 'users/user_avatar1605817739.png', NULL, '$2y$10$5hKJ9iMPF89dBDWJtLJQ/uRaw6/xiI/qGyOL4mYu8vnE9WUNyFN2W', NULL, NULL, '2020-11-20 01:29:00', '2020-11-20 01:30:24'),
(44, 3, 'Tristan', 'Perez', 'm', '1981-01-20', 0, 1, 'perezguzmantristan@gmail.com', 'users/user_avatar1605870551.png', NULL, '$2y$10$jUxVflA5N4XVh.BRkIkO7eSFmcri05wnmizGDJqHXeP0sHCW3Pz6C', NULL, NULL, '2020-11-20 16:09:11', '2020-11-20 16:42:38'),
(45, 3, 'Ruben Dario', 'Cruz Morales', 'm', '1986-10-05', 1, 1, 'boby.chalton.05@gmail.com', 'users/user_avatar1605872506.png', NULL, '$2y$10$J8PVEBViyGGpJ2haZRClCeyEqRcObZKHaAiIgtW7/jnZ1NG0zmfw6', NULL, NULL, '2020-11-20 16:41:46', '2020-11-20 16:44:11'),
(46, 3, 'Francisco Javier', 'Sánchez Rodríguez', 'm', '1997-10-27', 0, 1, 'fran_nito_27@hotmail.com', 'users/user_avatar1605919398.png', NULL, '$2y$10$JM7O7i6o8C1Qn9vnngVv3.6rH4jfZDh/issXjD3.bEunxEv3Tmbzi', NULL, NULL, '2020-11-21 05:43:18', '2020-11-21 05:43:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` int(10) UNSIGNED DEFAULT NULL,
  `evento_id` int(10) UNSIGNED DEFAULT NULL,
  `entrada_id` int(10) UNSIGNED DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amigos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metodo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_transaccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'por procesar',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `cliente_id`, `evento_id`, `entrada_id`, `codigo`, `amigos`, `metodo`, `n_transaccion`, `pago`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, '11395z22VGhLjH', NULL, 'efectivo', '10', 'por-procesar', '2020-11-15 21:57:12', '2020-11-15 21:57:12'),
(2, 6, 1, 1, '1142qCWTml0aVa', NULL, 'efectivo', '10', 'por-procesar', '2020-11-19 00:55:07', '2020-11-20 18:52:36'),
(3, 9, 1, 1, '11457y9AVlitch', NULL, 'efectivo', '10', 'por-procesar', '2020-11-20 16:44:11', '2020-11-20 16:44:11'),
(4, 4, 1, 1, '1140bctLAyUsa7', NULL, 'efectivo', '10', 'por-procesar', '2020-11-22 02:03:24', '2020-11-22 02:03:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `puntos`
--
ALTER TABLE `puntos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_evento_id_foreign` (`evento_id`),
  ADD KEY `stocks_entrada_id_foreign` (`entrada_id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ventas_codigo_unique` (`codigo`),
  ADD KEY `ventas_cliente_id_foreign` (`cliente_id`),
  ADD KEY `ventas_evento_id_foreign` (`evento_id`),
  ADD KEY `ventas_entrada_id_foreign` (`entrada_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `puntos`
--
ALTER TABLE `puntos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stocks_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `ventas_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`),
  ADD CONSTRAINT `ventas_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
