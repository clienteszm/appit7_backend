-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-07-2020 a las 13:37:13
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `appit7`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `email`, `sexo`, `date`, `puntos`, `avatar`, `estado`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'ziu macayo lópez', 'zmacayo@gmail.com', 'f', '2002-07-12', NULL, 'users/default.png', 0, 2, '2020-07-12 09:50:39', '2020-07-12 09:50:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-06-30 12:54:42', '2020-06-30 12:54:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

CREATE TABLE `entradas` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `precio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `titulo`, `descripcion`, `precio`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 'Normal', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '10', 'normal', NULL, NULL),
(2, 'VIP', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '30', 'vip', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `fecha` date DEFAULT NULL,
  `hora_inicio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hora_final` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `nombre`, `descripcion`, `fecha`, `hora_inicio`, `hora_final`, `imagen`, `created_at`, `updated_at`) VALUES
(1, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-07-30', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL),
(2, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-07-31', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL),
(3, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-08-13', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL),
(4, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-08-06', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL),
(5, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-08-07', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL),
(6, 'Fiesta de Música comercial', 'Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut just\'o malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan. Curabitur ut justo malesuada, vehicula elit eu, accumsan.', '2020-08-08', '21:00', '6:00', 'eventos/event.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-30 12:54:43', '2020-06-30 12:54:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-06-30 12:54:43', '2020-06-30 12:54:43', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-06-30 12:54:43', '2020-06-30 12:54:43', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-06-30 12:54:45', '2020-06-30 12:54:45', 'voyager.hooks', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_06_29_135152_add_surnames_to_users_table', 2),
(10, '2016_01_01_000000_add_voyager_user_fields', 3),
(11, '2016_01_01_000000_create_data_types_table', 3),
(12, '2016_05_19_173453_create_menu_table', 3),
(13, '2016_10_21_190000_create_roles_table', 3),
(14, '2016_10_21_190000_create_settings_table', 3),
(15, '2016_11_30_135954_create_permission_table', 3),
(16, '2016_11_30_141208_create_permission_role_table', 3),
(17, '2016_12_26_201236_data_types__add__server_side', 3),
(18, '2017_01_13_000000_add_route_to_menu_items_table', 3),
(19, '2017_01_14_005015_create_translations_table', 3),
(20, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 3),
(21, '2017_03_06_000000_add_controller_to_data_types_table', 3),
(22, '2017_04_21_000000_add_order_to_data_rows_table', 3),
(23, '2017_07_05_210000_add_policyname_to_data_types_table', 3),
(24, '2017_08_05_000000_add_group_to_settings_table', 3),
(25, '2017_11_26_013050_add_user_role_relationship', 3),
(26, '2017_11_26_015000_create_user_roles_table', 3),
(27, '2018_03_11_000000_add_user_settings', 3),
(28, '2018_03_14_000000_add_details_to_data_types_table', 3),
(29, '2018_03_16_000000_make_settings_value_nullable', 3),
(30, '2020_07_07_175109_add_statu_to_users_table', 4),
(40, '2020_07_07_175132_create_clientes_table', 5),
(41, '2020_07_07_175329_create_events_table', 5),
(42, '2020_07_07_175514_create_entradas_table', 5),
(43, '2020_07_07_175728_create_stocks_table', 5),
(44, '2020_07_07_175753_create_ventas_table', 5),
(45, '2020_07_14_115705_add_metodo_to_ventas_table', 6),
(46, '2020_07_14_115814_create_puntos_table', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('02b57ee6cef7cfb91ae8ce88da25148d77f362290bdc9682b7451a708336691b31a4760bf4681e50', 3, 1, 'authToken', '[]', 0, '2020-07-05 17:39:05', '2020-07-05 17:39:05', '2021-07-05 18:39:05'),
('045e60fa2c47d79c8fe6c05d69dd5e7050c3014ee33ba363f3d535db5524cabf5a00ca86b5372b72', 11, 1, 'authToken', '[]', 0, '2020-07-02 17:33:40', '2020-07-02 17:33:40', '2021-07-02 18:33:40'),
('05773e7ab803a42cf7b3dafa699944e1c542d0ef71fc7dfbf039c3fae492c85999d7f51372736c74', 5, 1, 'authToken', '[]', 0, '2020-07-02 14:37:59', '2020-07-02 14:37:59', '2021-07-02 15:37:59'),
('06cd637a11ea51beac5ca41ae4d8f43b0f246a5f214b1b0037458b2acd0646de539a0bffc24f9514', 10, 1, 'authToken', '[]', 0, '2020-07-02 17:23:23', '2020-07-02 17:23:23', '2021-07-02 18:23:23'),
('240d537f2927c7de63bad41fb93c5d515c5bc7b56dac485df485ff30200f3ec003f10241eda67a41', 5, 1, 'authToken', '[]', 0, '2020-07-02 14:37:57', '2020-07-02 14:37:57', '2021-07-02 15:37:57'),
('24ee13478aacdaf016201ed27f398b23752f47247714415061f2e35a01d9e16febead6b48a74eeb7', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:18:46', '2020-07-10 11:18:46', '2021-07-10 12:18:46'),
('29b688b041c332d099b58ec7111017d901022804f8fbe28238c71e540f57d939879e32bffbfaf235', 9, 1, 'authToken', '[]', 0, '2020-07-02 17:02:57', '2020-07-02 17:02:57', '2021-07-02 18:02:57'),
('2dbdb3b5af2cd56a3630aa39eb5229106c0e3cd4a3e41a0e976435d38d1b3f7e18e8757cc8793db6', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:08:20', '2020-07-10 11:08:20', '2021-07-10 12:08:20'),
('30b0c2d9b059a56229b7a67f78a7c1bf77d1a7774fec8750974b9422c95575cb537b3e8bed52ce5d', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:49:53', '2020-07-10 11:49:53', '2021-07-10 12:49:53'),
('32f984e3e489014fb7084ccbf3448a3a4c9c756249ba0f64bcbea930ec7b9b1493429018d062120a', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:46:01', '2020-07-10 11:46:01', '2021-07-10 12:46:01'),
('3922a9d4037f85ebeada692ab92ee169318f5f6314836261067ad36d24bcd7cd85d9954eeac8a430', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:16:44', '2020-07-10 11:16:44', '2021-07-10 12:16:44'),
('3d193653850e29c52d27ff9ce99ca34f8486d0e615f098385abb3ee3aedd83b2b252a29041a267be', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:03:14', '2020-07-02 17:03:14', '2021-07-02 18:03:14'),
('3ff6c91425efc286011ea7da58cb226eb454711641ba7c046bd52a1bd60b6222c3e0dd2a7e824ade', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:18:32', '2020-06-30 20:18:32', '2021-06-30 21:18:32'),
('4224b85471f8cd687d400b5941695cdab51ec4dc61cf059706b0749363f11ebc5e11a2bf5dad6bdf', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:10:19', '2020-06-30 20:10:19', '2021-06-30 21:10:19'),
('4656d7663d05f65da7cf15b7935e57f1f6ac1bc20b6287d251acde8e146a00841f8dfd9bf461a8d0', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:19:16', '2020-06-30 19:19:16', '2021-06-30 20:19:16'),
('5a9c9f25638737f215329f0272270401e107d874f886653aebb38ba2b9625b5f56447de49cdb5e0c', 2, 1, 'authToken', '[]', 0, '2020-07-12 09:50:39', '2020-07-12 09:50:39', '2021-07-12 10:50:39'),
('5e02dc722d2f89b61cb51f3173675676d314ece8f9d3cdc5c8f3e5247fe776ae26ff99bf50b9fdbf', 3, 1, 'authToken', '[]', 0, '2020-07-02 14:07:45', '2020-07-02 14:07:45', '2021-07-02 15:07:45'),
('5e13b68a8fe1055a828b0c92a51bbc48741785c7dced832fb406f1568ab9f393cb0312104f13a202', 3, 1, 'authToken', '[]', 0, '2020-07-10 10:49:32', '2020-07-10 10:49:32', '2021-07-10 11:49:32'),
('5e9229e730ded457068a3cf5a8108ee9f649cee7a07174d7cfd1c59f2d22db6cb9c8a3142b58f732', 3, 1, 'authToken', '[]', 0, '2020-07-10 10:45:25', '2020-07-10 10:45:25', '2021-07-10 11:45:25'),
('5f9098ae8dd8eca194cd16d422f70e95def5f638b0858a937baf6436eb83883f25f44477dc6728d7', 7, 1, 'authToken', '[]', 0, '2020-07-02 15:37:38', '2020-07-02 15:37:38', '2021-07-02 16:37:38'),
('622599fa151fdd9dedcae4965f9c578270e341aea5414e129a26d9afb396126e2d0912425714d100', 1, 1, 'authToken', '[]', 0, '2020-07-02 16:06:54', '2020-07-02 16:06:54', '2021-07-02 17:06:54'),
('69c666e92faeca5f474115d85c0865c24b0ec09b40e20f98ded3e527c5a5c17fb8a73d90178d2a53', 7, 1, 'authToken', '[]', 0, '2020-07-10 15:21:15', '2020-07-10 15:21:15', '2021-07-10 16:21:15'),
('6a252994fa0527737acae10f64ccc540ebe94ff47755c684ac3aae564261a75a3aa957e5e2f6097e', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:16:40', '2020-06-30 19:16:40', '2021-06-30 20:16:40'),
('6e045a6c3980ca65d3d599cd87e5660c72a59cd364669e82554d744d1cc6e081562fae41218d0e0a', 3, 1, 'authToken', '[]', 0, '2020-07-02 14:07:43', '2020-07-02 14:07:43', '2021-07-02 15:07:43'),
('6f671551d1d31b2e1f29cc1ea2e1e2952004f9ae34826789e4bd6aefbe9a4021e41944b9bbac6233', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:04:27', '2020-06-30 20:04:27', '2021-06-30 21:04:27'),
('72f31fec56db3a4191741aa0851af48f37e5dd41ddc977b82efb6a5ecdf355c95e73992170fdbad7', 1, 1, 'authToken', '[]', 0, '2020-07-02 09:09:31', '2020-07-02 09:09:31', '2021-07-02 10:09:31'),
('7951efd8f99868c8ea07e3341b44b678c10c6583b075efec2faa547ae5f5ebbdfdfd7ed6ad9a39c7', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:24:24', '2020-07-10 11:24:24', '2021-07-10 12:24:24'),
('7ad885ba2f3bed124080b05eca51acb5dbec100da6fa63a93fd102a227facb94a0a2558cc3a3bd42', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:01:26', '2020-07-02 17:01:26', '2021-07-02 18:01:26'),
('888f33d90ebe583353e20809880bf36027d5c63e9dbec58c2f03e25131404d8e519b4878aafa93a0', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:31:41', '2020-07-10 11:31:41', '2021-07-10 12:31:41'),
('8bb6343952c6684290077f630828a391bd220c43070628483e5f30a8aa99e1dc94da650790de34b2', 6, 1, 'authToken', '[]', 0, '2020-07-02 15:33:57', '2020-07-02 15:33:57', '2021-07-02 16:33:57'),
('8dafa436e00ce7df0c31e2cd7ac328ca5f540aa859fea888f71afbb83318ea5d10f5c57761e697fa', 9, 1, 'authToken', '[]', 0, '2020-07-10 15:23:14', '2020-07-10 15:23:14', '2021-07-10 16:23:14'),
('8f85a217347a22de3439937a1c147f4ac488e86e00d367f3af046de19aca967f69f3b4f8de38d574', 7, 1, 'authToken', '[]', 0, '2020-07-02 16:25:29', '2020-07-02 16:25:29', '2021-07-02 17:25:29'),
('95907f667196d5b27b620fdf2cb9860381b57fe9b97da1a5f5ece12099dd7e950f0707f905095ef9', 4, 1, 'authToken', '[]', 0, '2020-07-02 14:28:47', '2020-07-02 14:28:47', '2021-07-02 15:28:47'),
('96d3def359293efb128109dbba35ee0d07c975b8023146eeea932f6640f9f05bd43fc04250733119', 6, 1, 'authToken', '[]', 0, '2020-07-02 15:33:55', '2020-07-02 15:33:55', '2021-07-02 16:33:55'),
('9e97e466d647ed71d1f94b571d40eb77cad431b8566e9b37d34bad6c60d4af8e6162595311198711', 7, 1, 'authToken', '[]', 0, '2020-07-02 16:18:04', '2020-07-02 16:18:04', '2021-07-02 17:18:04'),
('a1986673d6b3c6f5fa88c70fd739b62d477b3faef0861587af330007380a0ea0c249a6d789ca1877', 10, 1, 'authToken', '[]', 0, '2020-07-02 17:23:25', '2020-07-02 17:23:25', '2021-07-02 18:23:25'),
('a245903551072ce39d040fe509a1132ebe335b0088dd9443bc87a8793375d04462876fd91a08611c', 4, 1, 'authToken', '[]', 0, '2020-07-02 14:28:54', '2020-07-02 14:28:54', '2021-07-02 15:28:54'),
('abaeecb7f35efdc740feb5a2f36394e707639b37df218265d8f6c2a803ccb0e39d62a9cf086c088b', 12, 1, 'authToken', '[]', 0, '2020-07-02 17:34:59', '2020-07-02 17:34:59', '2021-07-02 18:34:59'),
('add947db0ebe10e5bc3b0d530ff68efeca4c968daef603e1ab9fad2d8597a00a7d2fdc31f31ddaea', 1, 1, 'authToken', '[]', 0, '2020-06-30 20:18:18', '2020-06-30 20:18:18', '2021-06-30 21:18:18'),
('b2981818310ea2f0d6eeab0177be11542146065a27c6b5ff4e6bfb030a5e6cd7ab171e513838aebe', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:31:49', '2020-06-30 19:31:49', '2021-06-30 20:31:49'),
('b321c4195a8723ae41eefa991523a296e392eaaa81719281573956660ca95a937e8bd37728107dcc', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:00:35', '2020-07-02 17:00:35', '2021-07-02 18:00:35'),
('b73c9a42b4e35d100cfd245195cca3f5ca21ede3244af5686b5647dfce48bf9c1bba99123e06c3cd', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:14:29', '2020-07-10 11:14:29', '2021-07-10 12:14:29'),
('b89bde0f49ca65bae4045f93f970fd8bfa7820eb285de1d000689255ee88b9922f5e867049de2284', 6, 1, 'authToken', '[]', 0, '2020-07-10 15:07:12', '2020-07-10 15:07:12', '2021-07-10 16:07:12'),
('c1298b7490359bd18e521c1ce25e463964c57a994f516c1b9bb962d0c1bd125af355994843d87975', 3, 1, 'authToken', '[]', 0, '2020-07-03 18:11:31', '2020-07-03 18:11:31', '2021-07-03 19:11:31'),
('c1f79e83b17a921a238e9c75fdb6dc9be6251bf044161e24e003b6b479c37809f92b045767e0f2b8', 1, 1, 'authToken', '[]', 0, '2020-06-30 19:46:58', '2020-06-30 19:46:58', '2021-06-30 20:46:58'),
('c83ccf747a96d3396ae1c51bb39fa3cdeee38f508c7303ead5dbdc18adb6ebb802a688223ead1333', 4, 1, 'authToken', '[]', 0, '2020-07-10 15:00:19', '2020-07-10 15:00:19', '2021-07-10 16:00:19'),
('c86a59f92993e2fdbbd7f7eb833aff7722d387de90bee3a245969c3f1175e021f72d7ff2ec48b73e', 3, 1, 'authToken', '[]', 0, '2020-07-05 17:39:33', '2020-07-05 17:39:33', '2021-07-05 18:39:33'),
('c878091b2fbcc863dfd0275f0e9e1e61a1d4039bb3300ea6b49b4e1da88c52d7befbcb5cbb67b4ef', 9, 1, 'authToken', '[]', 0, '2020-07-02 17:02:59', '2020-07-02 17:02:59', '2021-07-02 18:02:59'),
('d6a609eb5cd172d17b4a952bf84feb15ebecd5170f0c8e15c565387bc4faa4c378083a85d5a8121a', 8, 1, 'authToken', '[]', 0, '2020-07-10 15:22:41', '2020-07-10 15:22:41', '2021-07-10 16:22:41'),
('db8eedac55f381c0a8b70b2f4f7bced656480883319a72c7b65010f919f27942d3213733678b41de', 12, 1, 'authToken', '[]', 0, '2020-07-02 17:34:57', '2020-07-02 17:34:57', '2021-07-02 18:34:57'),
('dff9d89237c55096b3b544370ffc812014224d65bda337aa9c95c91344b489259acf571d24c5f871', 11, 1, 'authToken', '[]', 0, '2020-07-02 17:33:49', '2020-07-02 17:33:49', '2021-07-02 18:33:49'),
('e1dd3332dc8f585909454a644e5d725ef84ac985e4e9a3e54b4c5a78bb46ac411700f4e46fed3325', 8, 1, 'authToken', '[]', 0, '2020-07-02 17:00:38', '2020-07-02 17:00:38', '2021-07-02 18:00:38'),
('e4434ff51d461c23022d82c8668e618735c02ad5592fd57b70b53ec4b9e80d3fccc8fa277fc02168', 3, 1, 'authToken', '[]', 0, '2020-07-05 16:32:17', '2020-07-05 16:32:17', '2021-07-05 17:32:17'),
('e776cb1fa06ac96f343395e6dc5c3f7d7dd24228291c7e13e13bbf659344eba6d73161ae96a00b13', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:09:50', '2020-07-10 11:09:50', '2021-07-10 12:09:50'),
('e78585bfdcbba7a5757b67dc15c906859d49695e7964ceb30a7d07269e4d2109a4acf5bbd1f6eda2', 5, 1, 'authToken', '[]', 0, '2020-07-10 15:05:15', '2020-07-10 15:05:15', '2021-07-10 16:05:15'),
('f12db7947770d22ab33fa12a83ccfcaea6f38eb839b31d30e27eb76eca576e0ac60ab49fa78c2541', 3, 1, 'authToken', '[]', 0, '2020-07-10 11:25:54', '2020-07-10 11:25:54', '2021-07-10 12:25:54'),
('f4787343373dc12ad9fefc2d460646ff73a4821b0f4e9c7c376bef50350f2aa7d980d26b66687091', 7, 1, 'authToken', '[]', 0, '2020-07-02 15:37:37', '2020-07-02 15:37:37', '2021-07-02 16:37:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '2XtkPqrBS5ZBF4cYkx2DsY6V2AOXZamyuTqxz8Ss', NULL, 'http://localhost', 1, 0, 0, '2020-06-29 12:15:35', '2020-06-29 12:15:35'),
(2, NULL, 'Laravel Password Grant Client', 'sHLRE2DcbP0GWhWvw1sthL8ijWHJGqkeW5HliS0a', 'users', 'http://localhost', 0, 1, 0, '2020-06-29 12:15:35', '2020-06-29 12:15:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-29 12:15:35', '2020-06-29 12:15:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(2, 'browse_bread', NULL, '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(3, 'browse_database', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(4, 'browse_media', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(5, 'browse_compass', NULL, '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(6, 'browse_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(7, 'read_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(8, 'edit_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(9, 'add_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(10, 'delete_menus', 'menus', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(11, 'browse_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(12, 'read_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(13, 'edit_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(14, 'add_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(15, 'delete_roles', 'roles', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(16, 'browse_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(17, 'read_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(18, 'edit_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(19, 'add_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(20, 'delete_users', 'users', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(21, 'browse_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(22, 'read_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(23, 'edit_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(24, 'add_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(25, 'delete_settings', 'settings', '2020-06-30 12:54:44', '2020-06-30 12:54:44'),
(26, 'browse_hooks', NULL, '2020-06-30 12:54:45', '2020-06-30 12:54:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntos`
--

CREATE TABLE `puntos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `porcentaje` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `puntos`
--

INSERT INTO `puntos` (`id`, `porcentaje`, `cantidad`, `estado`, `created_at`, `updated_at`) VALUES
(1, '5', 20, 1, NULL, NULL),
(2, '10', 30, 1, NULL, NULL),
(3, '25', 50, 1, NULL, NULL),
(4, '50', 100, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(2, 'user', 'Normal User', '2020-06-30 12:54:43', '2020-06-30 12:54:43'),
(3, 'usuario', 'Usuario App', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `evento_id` int(10) UNSIGNED DEFAULT NULL,
  `entrada_id` int(10) UNSIGNED DEFAULT NULL,
  `limite` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surnames` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `puntos` int(11) NOT NULL DEFAULT '0',
  `estado` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `surnames`, `sexo`, `date`, `puntos`, `estado`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', NULL, NULL, NULL, 0, 0, 'admin@appit7.com', 'users/default.png', NULL, '$2y$10$Qc/QalnlGIRI2uKA.au13Ojwz9zZPJ11ylWBKPDAte6j4ZeWAce7a', NULL, NULL, '2020-06-30 13:03:36', '2020-06-30 13:03:36'),
(2, 3, 'ziu', 'macayo lópez', 'f', '2002-07-12', 16, 1, 'zmacayo@gmail.com', 'users/default.png', NULL, '$2y$10$jqlaFMnlTMTp80C9dF.YpekygyZ6d8wB51CCbp6djwl.puWPoZaxW', NULL, NULL, '2020-07-12 09:50:39', '2020-07-15 06:03:36'),
(3, 2, 'user', NULL, NULL, NULL, 0, 1, 'user@appit7.com', 'users/default.png', NULL, '$2y$10$HLg92AHseJ74SkKn8et5p.nStuxQi68UO4xz9zFmlkf.olGIsSawa', NULL, '{\"locale\":\"en\"}', '2020-07-17 09:43:02', '2020-07-17 09:43:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` int(10) UNSIGNED DEFAULT NULL,
  `evento_id` int(10) UNSIGNED DEFAULT NULL,
  `entrada_id` int(10) UNSIGNED DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amigos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metodo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_transaccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'por procesar',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `cliente_id`, `evento_id`, `entrada_id`, `codigo`, `amigos`, `metodo`, `n_transaccion`, `pago`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 2, '312Fsd9qeh7Fd', '12345,12345,sdsd,msss,1234', 'efectivo', '10', 'por-procesar', '2020-07-15 06:03:35', '2020-07-16 17:42:33');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `puntos`
--
ALTER TABLE `puntos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_evento_id_foreign` (`evento_id`),
  ADD KEY `stocks_entrada_id_foreign` (`entrada_id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ventas_codigo_unique` (`codigo`),
  ADD KEY `ventas_cliente_id_foreign` (`cliente_id`),
  ADD KEY `ventas_evento_id_foreign` (`evento_id`),
  ADD KEY `ventas_entrada_id_foreign` (`entrada_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `entradas`
--
ALTER TABLE `entradas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `puntos`
--
ALTER TABLE `puntos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stocks_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `ventas_entrada_id_foreign` FOREIGN KEY (`entrada_id`) REFERENCES `entradas` (`id`),
  ADD CONSTRAINT `ventas_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
