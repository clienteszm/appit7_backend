<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
  protected $fillable = [
      'cliente_id', 'evento_id', 'entrada_id', 'codigo', 'amigos', 'metodo', 'n_transaccion', 'reservacion','pago'
  ];
}
