<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
  protected $fillable = [
      'user_id', 'name', 'sexo', 'date', 'email', 'avatar', 'puntos'
  ];
}
