<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Entrada;

class EntradaController extends Controller
{

    public function index()
    {
      $entradas = Entrada::all();

      return response()->json([
          'entradas' => $entradas,
      ]);
    }

    public function show($id)
    {
      $entrada = Entrada::find($id);

      return response()->json([
          'entrada' => $entrada,
      ]);
    }

}
