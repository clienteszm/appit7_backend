<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use App\Cliente;
use App\Venta;
use Session;

use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public function index()
    {
         //$accessToken = Session::get('accessToken');
         $accessToken = session('accessToken');

          $hoy = date('Y-m-d');
          $eventos = Event::where('fecha', '>=', $hoy)->orderBy('fecha','asc')->get();

          $ventas = Venta::all();
          $clientes = Cliente::all();

          return response()->json([
              'eventos' => $eventos,
              'ventas' => $ventas,
              'clientes' => $clientes,
              'accessToken' => $accessToken
          ]);
    }

    public function futuros($id)
    {
         //$accessToken = Session::get('accessToken');
         $accessToken = session('accessToken');

          $hoy = date('Y-m-d');

          $cliente = Cliente::where('user_id', $id)->first();
          $eventos = Event::where('fecha', '>=', $hoy)->orderBy('fecha','asc')->get();
          $ventas = Venta::where('cliente_id', $cliente->id)->get();

          $futuros = DB::table('ventas')
                          ->join('events', 'events.id', '=', 'ventas.evento_id')
                          ->where('events.fecha', '>=' , $hoy)
                          ->orderBy('events.fecha','asc')
                          ->where('ventas.cliente_id', $cliente->id)
                          ->select('events.*', 'ventas.id as venta')
                          ->get();

          if($futuros->count() == 0){
            $message = 'vacio';
          }else {
            $message = 'con eventos';
          }

          return response()->json([
              'eventos' => $futuros,
              'message' => $message,
              'accessToken' => $accessToken
          ]);
    }

    public function pasados($id)
    {
         //$accessToken = Session::get('accessToken');
         $accessToken = session('accessToken');

          $hoy = date('Y-m-d');

          $cliente = Cliente::where('user_id', $id)->first();
          $eventos = Event::where('fecha', '<', $hoy)->orderBy('fecha','asc')->get();
          $ventas = Venta::where('cliente_id', $cliente->id)->get();

          $pasados = DB::table('ventas')
                          ->join('events', 'events.id', '=', 'ventas.evento_id')
                          ->where('events.fecha', '<' , $hoy)
                          ->orderBy('events.fecha','asc')
                          ->where('ventas.cliente_id', $cliente->id)
                          ->select('events.*', 'ventas.id as venta')
                          ->get();

            if($pasados->count() == 0){
              $message = 'vacio';
            }else {
              $message = 'con eventos';
            }


          return response()->json([
              'eventos' => $pasados,
              'message' => $message,
              'accessToken' => $accessToken
          ]);
    }


    public function show($id)
    {
        $evento = Event::find($id);

        $reservadoVip = Venta::where('evento_id', $id)->where('reservacion', 'Reservado VIP')->first();
        $reservado1 = Venta::where('evento_id', $id)->where('reservacion', 'Mesa Vista, 1')->first();
        $reservado2 = Venta::where('evento_id', $id)->where('reservacion', 'Mesa Vista, 2')->first();
        $reservado3 = Venta::where('evento_id', $id)->where('reservacion', 'Mesa Vista, 3')->first();
        $reservado4 = Venta::where('evento_id', $id)->where('reservacion', 'Mesa Vista, 4')->first();
        $reservado5 = Venta::where('evento_id', $id)->where('reservacion', 'Mesa Vista, 5')->first();

        return response()->json([
          'evento' => $evento,
          'reservadoVip' => $reservadoVip,
          'reservado1' => $reservado1,
          'reservado2' => $reservado2,
          'reservado3' => $reservado3,
          'reservado4' => $reservado4,
          'reservado5' => $reservado5,
        ]);
    }

    public function venta($id)
    {
        $venta = Venta::find($id);

        return response()->json([
            'venta' => $venta,
        ]);
    }

    public function friends($f, $v){
      $venta = Venta::find($v);
      $venta->amigos = $f;

      $venta->save();

      return response()->json('Exit');

    }


}
