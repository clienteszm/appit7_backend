<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Cliente;
use App\Event;
use App\Punto;
use Image;

use Mail;
use Session;
use Illuminate\Support\Str;

use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;

class RegisterController extends Controller
{

    public function register(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'surnames' => 'required|string|max:255',
            'sexo' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $image_service_str = substr($request->avatar, strpos($request->avatar, ",")+1);
        $img = base64_decode($image_service_str);

        $img_extension = 'png';
        $img_name = 'user_avatar'. time() . '.' . $img_extension;
        Storage::disk('users')->put($img_name, $img);

        $date = date('Y/m/d', strtotime($request->date));

        $user = User::create([
            'role_id' => 3,
            'name' => $request->name,
            'surnames' => $request->surnames,
            'sexo' => $request->sexo,
            'date' => $date,
            'avatar' => 'users/'.$img_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

         Cliente::create([
            'user_id' => $user->id,
            'name' => $request->name.' '.$request->surnames,
            'sexo' => $request->sexo,
            'date' => $date,
            'avatar' => $user->avatar,
            'email' => $request->email,
            'puntos' => $user->punto,
        ]);

        $accessToken = $user->createToken('authToken')->accessToken;

        $correo = 'danielensen@gmail.com';
        $subject = 'Nuevo usuario registrado.';
        HelperCorreo::sendMail('emails.usuario', $user, $correo, $subject);

        return response()->json([
            'message' => 'Usuario creado correctamente',
            'user' => $user->toArray(),
            'access_token' => $accessToken,
        ]);

    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if(!auth()->attempt($loginData)) {
            return response(['message'=>'Credenciales invalidas']);
        }

      //   $accessToken = auth()->user()->createToken('authToken')->accessToken;
        $accessToken = Str::random(40);

      //  Session::put('accessToken ', $accessToken);
        session(['accessToken' => $accessToken]);
        //session('accessToken', $accessToken);

        $user = auth()->user();

        $cliente = Cliente::where('user_id', $user->id)->first();

        $porcentaje =  $this->porcentaje($user->puntos);
        $puntos = Punto::where('estado', 1)->get();

        if($user->estado == 0 && $cliente->estado == 1){
          $user->estado = 1;
          $user->save();
        }

        if($user->estado == 1){
          return response()->json(['user' => $user, 'access_token' => $accessToken, 'porcentaje' => $porcentaje, 'puntos' => $puntos]);
        }else{
          return response(['message'=>'Registro no aprobado']);
        }
    }

    public function contactanos(Request $request)
    {

      $contacto = New \App\Contacto;
      $contacto->nombre = $request->nombre;
      $contacto->email = $request->email;
      $contacto->asunto = $request->asunto;
      $contacto->mensaje = $request->mensaje;
      $contacto->save();

      return response()->json(['user' => 'Enviado con éxito',]);

    }

    public function porcentaje($puntos_user)
    {
        $porcentaje = 0;

        $puntos = Punto::where('estado', 1)->get();

        for($i=0; $i<count($puntos); $i++){
          if($puntos_user >= $puntos[$i]->cantidad && $puntos_user < $puntos[$i+1]->cantidad){
            $porcentaje = $puntos[$i]->porcentaje;
          }
        }
        return $porcentaje;

    }


    public function edit(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'surnames' => 'required|string|max:255',
        ]);

      // Obtener los datos de la imagen
      $img_name = 'default.png';
      if($request->avatar){
        $image_service_str = substr($request->avatar, strpos($request->avatar, ",")+1);
        $img = base64_decode($image_service_str);

        $img_extension = 'png';
        $img_name = 'user_avatar'. time() . '.' . $img_extension;
        Storage::disk('users')->put($img_name, $img);
      }


        $date = date('Y/m/d', strtotime($request->date));

        $user = User::find($request->user_id);

        $user->name = $request->name;
        $user->surnames = $request->surnames;
        $user->avatar = 'users/'.$img_name;
        if($request->password){
          $user->password = bcrypt($request->password);
        }

        $cliente = Cliente::where('user_id', $request->user_id)->first();

        $cliente->name = $request->name.' '.$request->surnames;
        $cliente->avatar = 'users/'.$img_name;

        $porcentaje =  $this->porcentaje($user->puntos);
        $puntos = Punto::where('estado', 1)->get();

        $user->save();
        $cliente->save();

        return response()->json([
            'message' => 'Perfil editado correctamente',
            'user' => $user->toArray(),
            'porcentaje' => $porcentaje,
            'puntos' => $puntos,
        ]);
    }

    public function enviar(Request $request){

      $user = User::where('email', $request->correo)->first();

      if($user){
        $correo = $request->correo;
        $subject = 'Código '.$request->codigo;
        HelperCorreo::sendMail('emails.recuperacion', $request, $correo, $subject);

        return response()->json('Código enviado exitosamente.');
      }else{
        return response()->json('Usuario no existe.');
      }

    }

    public function actualizar(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|string|min:6',
        ]);

        $user = User::where('email', $request->correo)->first();
        if($user){
          $user->password = bcrypt($request->password);

          $user->save();

          $accessToken = $user->createToken('authToken')->accessToken;

          return response()->json([
              'message' => 'Contraseña actualizada exitosamente.',
              'user' => $user,
              'access_token' => $accessToken,
          ]);
        }else{
          return response()->json([
              'message' => 'Error',
              'user' => $user,
              'email' => $request->correo,
          ]);
        }

    }

    public function logout(){
      session()->forget('accessToken');
    }

    public function index($token, Request $request)
    {
       //  $accessToken = Session::get('accessToken');
         $accessToken = session('accessToken');

        if($token == $accessToken){
          $hoy = date('Y-m-d');
          $eventos = Event::where('fecha', '>=', $hoy)->orderBy('fecha','asc')->get();

          return response()->json([
              'eventos' => $eventos,
          ]);
        }else{
          return response()->json([
              'accessToken' => session()->has('accessToken'),
              'token' => $token,
          ]);
        }
    }

    public function aprobado($id){

      $user = User::find($id);
      if($user->estado != 1){
              $cliente = Cliente::where('user_id', $id)->first();
              $cliente->estado = 1;
              $user->estado = 1;

              $user->save();
              $cliente->save();

              $correo = $user->email;
              $subject = 'Tu registro en la app It7 ha sido aprobado.';
              HelperCorreo::sendMail('emails.aprobacion', $user, $correo, $subject);
      }

      return view('aprobacion', compact('user'));

    }


}
