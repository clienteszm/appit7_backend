<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Venta;
use App\Event;
use App\Entrada;
use App\Punto;
use App\User;
use App\Cliente;

use Mail;
use App\Helpers\Frontend\EnviosCorreo as HelperCorreo;

class VentaController extends Controller
{
    public function saveVenta(Request $request)
    {

        $user = User::find($request->preventa[2]);
        $cliente = Cliente::where('user_id', $user->id)->first();
        $entrada = Entrada::find($request->preventa[1]);
        $evento = Event::find($request->preventa[0]);

        if($request->reservacion == 0){
          $reservacion = 'Reservado VIP';
        }else{
          $reservacion = 'Mesa Vista, '. $request->reservacion;
        }

        $venta = Venta::create([
           'cliente_id' => $cliente->id,
           'evento_id' => $request->preventa[0],
           'entrada_id' => $request->preventa[1],
           'codigo' => $request->codigo,
           'amigos' => $request->amigos,
           'metodo' => $request->metodo,
           'n_transaccion' => $request->transaccion,
           'reservacion' => $reservacion,
           'pago' => $request->estado,
        ]);

       if($entrada->tipo == 'normal'){
         $user->puntos = $user->puntos + 1;
       }else{
         $user->puntos = $user->puntos + 2;
       }

       $user->save();

       $amigo = explode(",", $request->amigos);

       $array = [
            "nombre" => $user->name,
            "evento" => $evento->nombre,
            "entrada" => $entrada->titulo,
            "fecha" => $evento->fecha,
            "hora" => 'Desde las '.$evento->hora_inicio.' hasta las '.$evento->hora_final,
            "metodo" => $request->metodo,
            "n_transaccion" => $request->transaccion,
            'reservacion' => $reservacion,
            'amigo1' => $amigo[0],
            'amigo2' => $amigo[1],
            'amigo3' => $amigo[2],
            'amigo4' => $amigo[3],
            'amigo5' => $amigo[4],
            'amigo6' => $amigo[5],
            'amigo7' => $amigo[6],
            'amigo8' => $amigo[7],
            'amigo9' => $amigo[8],
            'amigo10' => $amigo[9],
            "pago" => $request->estado
        ];

       $correo = $user->email;
       $correo2 =  'danielensen@gmail.com';
       $subject = 'Tu compra ha sido exitosa.';
       $subject2 = 'Compra de entrada con '. $reservacion .', realizada con exito.';
       HelperCorreo::sendMail('emails.compra_exitosa', $array, $correo, $subject);
       HelperCorreo::sendMail('emails.compra_realizada', $array, $correo2, $subject2);

       return response()->json([
           'message' => 'Venta creada en base de datos',
           'venta' => $venta,

       ]);
    }


    public function obtener($preventa)
    {
       $preventa = explode(",", $preventa);

       $evento = Event::find($preventa[0]);
       $entrada = Entrada::where('id', $preventa[1])->first();
       $user = User::where('id',  $preventa[2])->first();

       $porcentaje = $this->porcentaje($user->puntos);

       return response()->json([
           'evento' => $evento,
           'entrada' => $entrada,
           'user' => $user,
           'porcentaje' => $porcentaje
       ]);

    }

    public function user($id){

      $user = User::find($id);

      $porcentaje =  $this->porcentaje($user->puntos);
      $puntos = Punto::where('estado', 1)->get();

      return response()->json([
          'user' => $user,
          'porcentaje' => $porcentaje,
          'puntos' => $puntos,
      ]);
    }

    public function porcentaje($puntos_user)
    {
        $porcentaje = 0;

        $puntos = Punto::where('estado', 1)->get();

        for($i=0; $i<count($puntos); $i++){
          if($puntos_user >= $puntos[$i]->cantidad && $puntos_user < $puntos[$i+1]->cantidad){
            $porcentaje = $puntos[$i]->porcentaje;
          }
        }
        return $porcentaje;

    }

    public function scanner($code){

      $venta = Venta::where('codigo', $code)->first();
      $evento = Event::where('id', $venta->evento_id)->first();
      $entrada = Entrada::where('id', $venta->entrada_id)->first();
      $cliente = Cliente::where('id', $venta->cliente_id)->first();

      return response()->json([
          'venta' => $venta,
          'evento' => $evento,
          'cliente' => $cliente,
          'entrada' => $entrada
      ]);

    }

    public function pagar($code){

      $venta = Venta::where('codigo', $code)->first();

      $venta->pago = 'Procesado';

      $venta->save();

      return response()->json("procesado");

    }


}
