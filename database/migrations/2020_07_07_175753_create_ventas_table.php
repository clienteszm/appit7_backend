<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('cliente_id')->nullable();
            $table->unsignedInteger('evento_id')->nullable();
            $table->unsignedInteger('entrada_id')->nullable();
            $table->string('codigo')->unique()->nullable();
            $table->string('amigos')->nullable();
            $table->string('pago')->default('por procesar');
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('evento_id')->references('id')->on('events');
            $table->foreign('entrada_id')->references('id')->on('entradas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
