<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSurnamesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->string('surnames')->after('name');
          $table->string('sexo')->after('surnames');
          $table->date('date')->after('sexo');
          $table->integer('puntos')->after('date')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropColumn('surnames');
          $table->dropColumn('sexo');
          $table->dropColumn('date');
          $table->dropColumn('puntos');
        });
    }
}
